<%@ page contentType="text/html; charset=UTF-8"%>
<%@ include file="init.jsp" %>


<liferay-portlet:actionURL var="uploadFileURL">
<liferay-portlet:param name="myActions" value="uploadMultipleFile" />
</liferay-portlet:actionURL>
 

<script>
jQuery(function(){ // wait for document to load
jQuery('#uploadFile').MultiFile({
STRING: {
	remove: '<img src="./images/bin.gif" height="16" width="16" alt="x"/>'
}
});
});
 
function uploadNewFile()
{
jQuery('#fileTags').find('input[type="file"]').each(
function() {
var id = jQuery(this).attr('id');
jQuery(this).attr('name',id);
});
document.multipleUpload.action='<liferay-portlet:actionURL ><liferay-portlet:param name="myActions" value="uploadMultipleFile"/></liferay-portlet:actionURL >';
document.multipleUpload.submit();
}
</script>
 
 
<form enctype="multipart/form-data" method="post" name="multipleUpload" class="upload-form">
<table id="fileTags">
<tbody>
<tr><td>Upload File</td>
<td><input class="multi" id="uploadFile" name="uploadFile" type="file" />
</td>
<td></td>
</tr>
</tbody></table>
<input onclick="uploadNewFile()" type="button" value="Upload" />
</form>
