
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/security" prefix="liferay-security" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x" %>


<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%-- 
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-bean-el" prefix="bean-el" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-html-el" prefix="html-el" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-logic-el" prefix="logic-el" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles-el" prefix="tiles-el" %>
 --%>

<%@ page contentType="text/html; charset=UTF-8" %>


<%@page import="java.io.Serializable" %>

<%@page import="java.math.BigDecimal" %>

<%@page import="java.net.InetAddress" %>
<%@page import="java.net.UnknownHostException" %>

<%@page import="java.text.DateFormat" %>
<%@page import="java.text.DecimalFormat" %>
<%@page import="java.text.Format" %>
<%@page import="java.text.MessageFormat" %>
<%@page import="java.text.NumberFormat" %>

<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Arrays" %>
<%@page import="java.util.Calendar" %>
<%@page import="java.util.Collection" %>
<%@page import="java.util.Collections" %>
<%@page import="java.util.Currency" %>
<%@page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@page import="java.util.GregorianCalendar" %>
<%@page import="java.util.HashMap" %>
<%@page import="java.util.HashSet" %>
<%@page import="java.util.Iterator" %>
<%@page import="java.util.LinkedHashMap" %>
<%@page import="java.util.LinkedHashSet" %>
<%@page import="java.util.List" %>
<%@page import="java.util.Locale" %>
<%@page import="java.util.Map" %>
<%@page import="java.util.Properties" %>
<%@page import="java.util.ResourceBundle" %>
<%@page import="java.util.Set" %>
<%@page import="java.util.Stack" %>
<%@page import="java.util.TimeZone" %>
<%@page import="java.util.TreeMap" %>
<%@page import="java.util.TreeSet" %>
<%@page import="java.io.StringReader"%>

<%@page import="javax.portlet.MimeResponse" %>
<%@page import="javax.portlet.PortletConfig" %>
<%@page import="javax.portlet.PortletContext" %>
<%@page import="javax.portlet.PortletException" %>
<%@page import="javax.portlet.PortletMode" %>
<%@page import="javax.portlet.PortletPreferences" %>
<%@page import="javax.portlet.PortletRequest" %>
<%@page import="javax.portlet.PortletResponse" %>
<%@page import="javax.portlet.PortletSession" %>
<%@page import="javax.portlet.PortletURL" %>
<%@page import="javax.portlet.RenderRequest" %>
<%@page import="javax.portlet.RenderResponse" %>
<%@page import="javax.portlet.ResourceURL" %>
<%@page import="javax.portlet.UnavailableException" %>
<%@page import="javax.portlet.ValidatorException" %>
<%@page import="javax.portlet.WindowState" %>


<%@page import="com.liferay.counter.service.CounterLocalServiceUtil" %>
<%@page import="com.liferay.portal.NoSuchUserException" %>
<%@page import="com.liferay.portal.kernel.captcha.CaptchaMaxChallengesException" %>
<%@page import="com.liferay.portal.kernel.captcha.CaptchaTextException" %>
<%@page import="com.liferay.portal.kernel.bean.BeanParamUtil" %>
<%@page import="com.liferay.portal.kernel.bean.BeanPropertiesUtil" %>
<%@page import="com.liferay.portal.kernel.cal.Recurrence" %>
<%@page import="com.liferay.portal.kernel.configuration.Filter" %>
<%@page import="com.liferay.portal.kernel.dao.search.AlwaysTrueRowChecker" %>
<%@page import="com.liferay.portal.kernel.dao.search.DAOParamUtil" %>
<%@page import="com.liferay.portal.kernel.dao.search.DisplayTerms" %>
<%@page import="com.liferay.portal.kernel.dao.search.JSPSearchEntry" %>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow" %>
<%@page import="com.liferay.portal.kernel.dao.search.RowChecker" %>
<%@page import="com.liferay.portal.kernel.dao.search.ScoreSearchEntry" %>
<%@page import="com.liferay.portal.kernel.dao.search.SearchContainer" %>
<%@page import="com.liferay.portal.kernel.dao.search.SearchEntry" %>
<%@page import="com.liferay.portal.kernel.dao.search.TextSearchEntry" %>
<%@page import="com.liferay.portal.kernel.exception.PortalException" %>
<%@page import="com.liferay.portal.kernel.exception.SystemException" %>
<%@page import="com.liferay.portal.kernel.io.unsync.UnsyncStringReader" %>
<%@page import="com.liferay.portal.kernel.json.JSONArray" %>
<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil" %>
<%@page import="com.liferay.portal.kernel.json.JSONObject" %>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil" %>
<%@page import="com.liferay.portal.kernel.language.LanguageWrapper" %>
<%@page import="com.liferay.portal.kernel.language.UnicodeLanguageUtil" %>
<%@page import="com.liferay.portal.kernel.log.Log" %>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil" %>
<%@page import="com.liferay.portal.kernel.log.LogUtil" %>
<%@page import="com.liferay.portal.kernel.messaging.DestinationNames" %>
<%@page import="com.liferay.portal.kernel.messaging.MessageBusUtil" %>
<%@page import="com.liferay.portal.kernel.portlet.DynamicRenderRequest" %>
<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletMode" %>
<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletRequest" %>
<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletResponse" %>
<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletURL" %>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState" %>
<%@page import="com.liferay.portal.kernel.search.Field" %>
<%@page import="com.liferay.portal.kernel.servlet.BrowserSnifferUtil" %>
<%@page import="com.liferay.portal.kernel.servlet.ImageServletTokenUtil" %>
<%@page import="com.liferay.portal.kernel.servlet.ServletContextUtil" %>
<%@page import="com.liferay.portal.kernel.servlet.SessionErrors" %>
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages" %>
<%@page import="com.liferay.portal.kernel.servlet.StringServletResponse" %>
<%@page import="com.liferay.portal.kernel.util.ArrayUtil" %>
<%@page import="com.liferay.portal.kernel.util.*" %>
<%@page import="com.liferay.portal.kernel.workflow.WorkflowConstants" %>
<%@page import="com.liferay.portal.model.*" %>
<%@page import="com.liferay.portal.model.impl.*" %>
<%@page import="com.liferay.portal.security.auth.AuthTokenUtil" %>
<%@page import="com.liferay.portal.security.auth.PrincipalException" %>
<%@page import="com.liferay.portal.security.permission.ActionKeys" %>
<%@page import="com.liferay.portal.security.permission.PermissionChecker" %>
<%@page import="com.liferay.portal.security.permission.ResourceActionsUtil" %>
<%@page import="com.liferay.portal.service.*" %>
<%@page import="com.liferay.portal.service.permission.GroupPermissionUtil" %>
<%@page import="com.liferay.portal.service.permission.LayoutPermissionUtil" %>
<%@page import="com.liferay.portal.service.permission.LayoutPrototypePermissionUtil" %>
<%@page import="com.liferay.portal.service.permission.LayoutSetPrototypePermissionUtil" %>
<%@page import="com.liferay.portal.service.permission.PortletPermissionUtil" %>
<%@page import="com.liferay.portal.theme.PortletDisplay" %>
<%@page import="com.liferay.portal.theme.ThemeDisplay" %>
<%@page import="com.liferay.portal.util.*" %>
<%@page import="com.liferay.portal.util.comparator.PortletCategoryComparator" %>
<%@page import="com.liferay.portal.util.comparator.PortletTitleComparator" %>
<%@page import="com.liferay.portlet.InvokerPortlet" %>
<%@page import="com.liferay.portlet.PortalPreferences" %>
<%@page import="com.liferay.portlet.PortletConfigFactoryUtil" %>
<%@page import="com.liferay.portlet.PortletInstanceFactoryUtil" %>
<%@page import="com.liferay.portlet.PortletPreferencesFactoryUtil" %>
<%@page import="com.liferay.portlet.PortletSetupUtil" %>
<%@page import="com.liferay.portlet.PortletURLUtil" %>
<%@page import="com.liferay.portlet.UserAttributes" %>
<%@page import="com.liferay.portlet.portletconfiguration.util.PortletConfigurationUtil" %>
<%@page import="com.liferay.util.format.PhoneNumberUtil" %>
<%@page import="com.liferay.util.log4j.Levels" %>
<%@page import="com.liferay.util.mail.InternetAddressUtil" %>
<%@page import="com.liferay.util.portlet.PortletRequestUtil" %>
<%@page import="com.liferay.util.servlet.DynamicServletRequest" %>
<%@page import="com.liferay.util.servlet.SessionParameters" %>
<%@page import="com.liferay.util.servlet.UploadException" %>
<%@page import="com.liferay.util.xml.XMLFormatter" %>
<%@page import="com.liferay.portal.kernel.search.Field" %>
<%@page import="com.liferay.portal.kernel.search.Hits" %>
<%@page import="com.liferay.portal.kernel.search.Indexer" %>
<%@page import="com.liferay.portal.kernel.search.IndexerRegistryUtil" %>
<%@page import="com.liferay.portal.kernel.search.SearchContext" %>
<%@page import="com.liferay.portal.kernel.search.SearchContextFactory" %>
<%@page import="com.liferay.portlet.asset.model.AssetCategory" %>
<%@page import="com.liferay.portlet.asset.model.AssetEntry" %>
<%@page import="com.liferay.portlet.asset.model.AssetRenderer" %>
<%@page import="com.liferay.portlet.asset.model.AssetVocabulary" %>
<%@page import="com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil" %>
<%@page import="com.liferay.portlet.asset.service.AssetEntryServiceUtil" %>
<%@page import="com.liferay.portlet.asset.service.AssetTagServiceUtil" %>
<%@page import="com.liferay.portlet.asset.service.AssetVocabularyLocalServiceUtil" %>
<%@page import="com.liferay.portlet.asset.service.persistence.AssetEntryQuery" %>
<%@page import="com.liferay.portlet.imagegallery.DuplicateFolderNameException" %>
<%@page import="com.liferay.portlet.imagegallery.DuplicateImageNameException" %>
<%@page import="com.liferay.portlet.imagegallery.FolderNameException" %>
<%@page import="com.liferay.portlet.imagegallery.ImageNameException" %>
<%@page import="com.liferay.portlet.imagegallery.ImageSizeException" %>
<%@page import="com.liferay.portlet.imagegallery.NoSuchFolderException" %>
<%@page import="com.liferay.portlet.imagegallery.NoSuchImageException" %>
<%@page import="com.liferay.portlet.imagegallery.model.IGFolder" %>
<%@page import="com.liferay.portlet.imagegallery.model.IGFolderConstants" %>
<%@page import="com.liferay.portlet.imagegallery.model.IGImage" %>
<%@page import="com.liferay.portlet.imagegallery.service.IGFolderLocalServiceUtil" %>
<%@page import="com.liferay.portlet.imagegallery.service.IGFolderServiceUtil" %>
<%@page import="com.liferay.portlet.imagegallery.service.IGImageLocalServiceUtil" %>
<%@page import="com.liferay.portlet.imagegallery.service.IGImageServiceUtil" %>
<%@page import="com.liferay.portal.kernel.util.PrefsPropsUtil" %>
<%@page import="com.liferay.portal.kernel.util.PropsUtil"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>

<%@page import="com.liferay.portal.kernel.search.OpenSearch" %>
<%@page import="com.liferay.portal.kernel.search.OpenSearchUtil" %>
<%@page import="com.liferay.portal.kernel.search.SearchException" %>
<%@page import="com.liferay.portal.kernel.xml.Element" %>
<%@page import="com.liferay.portal.kernel.xml.SAXReaderUtil" %>
<%@page import="com.liferay.portal.kernel.xml.Node"%>
<%@page import="com.liferay.portal.kernel.xml.Document"%>


<%@page import="com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil" %>
<%@page import="com.liferay.portlet.documentlibrary.model.DLFileEntry" %>
<%@page import="com.liferay.portlet.PortletURLFactoryUtil" %>
<%@page import="com.liferay.portlet.expando.model.ExpandoTableConstants" %>
<%@page import="com.liferay.portlet.expando.model.ExpandoValue" %>
<%@page import="com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil" %>
<%@page import="com.liferay.portal.service.ClassNameLocalServiceUtil" %>
<%@page import="com.liferay.portlet.expando.service.ExpandoColumnServiceUtil"%>

<%@page import="com.liferay.portlet.expando.util.ExpandoBridgeFactoryUtil"%>
<%@page import="com.liferay.portlet.expando.model.ExpandoBridge"%>
<%@page import="com.liferay.portal.kernel.dao.orm.*"%>
<%@page import="com.liferay.portlet.expando.service.ExpandoColumnLocalServiceUtil"%>
<%@page import="com.liferay.portlet.expando.model.ExpandoColumn"%>
<%@page import="com.liferay.util.portlet.PortletProps"%>
<%@page import="com.liferay.portlet.journal.service.JournalArticleServiceUtil"%>
<%@page import="com.liferay.portlet.journal.model.*"%>

<%@page import="org.slf4j.Logger"%>
<%@page import="org.slf4j.LoggerFactory"%>
<%@page import="org.apache.commons.math.util.MathUtils" %>
<%@page import="org.apache.commons.beanutils.BeanComparator"%>
<%@page import="org.apache.commons.collections.comparators.NullComparator"%>

<%@page import="gov.ny.dfs.hr.util.*"%>
<%@page import="gov.ny.dfs.hr.service.model.*"%>
<%@page import="gov.ny.dfs.hr.service.service.*"%>


<liferay-theme:defineObjects />

<portlet:defineObjects />

<%
WindowState windowState = null;
PortletMode portletMode = null;

PortletURL currentURLObj = null;

if (renderRequest != null) {
	windowState = renderRequest.getWindowState();
	portletMode = renderRequest.getPortletMode();

	currentURLObj = PortletURLUtil.getCurrent(renderRequest, renderResponse);
}
else if (resourceRequest != null) {
	windowState = resourceRequest.getWindowState();
	portletMode = resourceRequest.getPortletMode();

	currentURLObj = PortletURLUtil.getCurrent(resourceRequest, resourceResponse);
}

String currentURL = currentURLObj.toString();
//String currentURL = PortalUtil.getCurrentURL(request);

PortletPreferences preferences = renderRequest.getPreferences();

String portletResource = ParamUtil.getString(request, "portletResource");

if (Validator.isNotNull(portletResource)) {
	preferences = PortletPreferencesFactoryUtil.getPortletSetup(request, portletResource);
}
else if (layout.isTypeControlPanel()) {
	preferences = PortletPreferencesLocalServiceUtil.getPreferences(themeDisplay.getCompanyId(), scopeGroupId, PortletKeys.PREFS_OWNER_TYPE_GROUP, 0, PortletKeys.IMAGE_GALLERY, null);
}

long rootFolderId = PrefsParamUtil.getLong(preferences, request, "rootFolderId", IGFolderConstants.DEFAULT_PARENT_FOLDER_ID);

Format dateFormatDate = FastDateFormatFactoryUtil.getDate(locale, timeZone);

PortalPreferences portalPreferences = PortletPreferencesFactoryUtil.getPortalPreferences(request);

boolean dlLinkToViewURL = false;
boolean includeSystemPortlets = false;

String namespace = StringPool.BLANK;
if (renderResponse != null) {
	namespace = renderResponse.getNamespace();
}

%>

<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<%-- <script type="text/javascript" src="http://code.jquery.com/jquery.min.js" ></script> --%>
<%-- 
<script async="async" type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" ></script>
<script async="async" type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js" ></script>
<script async="async" type="text/javascript" src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.js" ></script>
 --%>
