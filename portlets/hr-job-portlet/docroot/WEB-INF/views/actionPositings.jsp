<%@ include file="init.jsp"%>
<%!private static final Logger _log = LoggerFactory.getLogger("gov.ny.dfs.hr.view.actionPositings_jsp");%>


<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import="com.liferay.portal.util.PortalUtil"%>

<portlet:defineObjects />

<%
	ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	Posting posting = (Posting) row.getObject();
	long postingId = posting.getPostingId();
%>

<liferay-ui:icon-menu>

	<liferay-portlet:renderURL var="viewPostingURL">
		<liferay-portlet:param name="path" value="viewPosting" />
		<liferay-portlet:param name="postingId" value="<%=String.valueOf(postingId) %>" />
	</liferay-portlet:renderURL>
	<liferay-ui:icon image="view" url="<%=viewPostingURL.toString()%>" label="posting-view" />

	<portlet:renderURL var="editURL">
		<portlet:param name="path" value="editPosting" />
		<portlet:param name="postingId" value="<%=String.valueOf(postingId)%>" />
	</portlet:renderURL>
	<liferay-ui:icon image="edit" url="<%=editURL.toString()%>" label="posting-edit" />

	<%
		String deletePostingURL = "javascript:"+ renderResponse.getNamespace() + "deletePosting('"+postingId+"')";
	%>
	<liferay-ui:icon image="delete" url="<%=deletePostingURL%>" label="posting-delete" />

</liferay-ui:icon-menu>
