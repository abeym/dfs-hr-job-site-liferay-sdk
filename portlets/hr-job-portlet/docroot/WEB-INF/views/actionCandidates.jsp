<%@ include file="init.jsp"%>
<%!private static final Logger _log = LoggerFactory.getLogger("gov.ny.dfs.hr.view.actionPositings_jsp");%>


<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import="com.liferay.portal.util.PortalUtil"%>

<portlet:defineObjects />

<%
	ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	Candidate candidate = (Candidate) row.getObject();

	String name = Candidate.class.getName();
	long candidateId = candidate.getCandidateId();

	String redirect = PortalUtil.getCurrentURL(renderRequest);
%>

<liferay-ui:icon-menu>
	<portlet:renderURL var="editURL">
		<portlet:param name="path" value="editCandidate" />
		<portlet:param name="candidateId" value="<%=String.valueOf(candidateId)%>" />
		<portlet:param name="redirect" value="<%=redirect%>" />
	</portlet:renderURL>

	<liferay-ui:icon label="candidate-edit" image="edit" url="<%=editURL.toString()%>" />

	<portlet:actionURL var="deleteURL">
		<portlet:param name="action" value="deleteCandidate" />
		<portlet:param name="candidateId" value="<%=String.valueOf(candidateId)%>" />
		<portlet:param name="redirect" value="<%=redirect%>" />
	</portlet:actionURL>

	<liferay-ui:icon-delete label="candidate-delete" url="<%=deleteURL.toString()%>" />

</liferay-ui:icon-menu>
