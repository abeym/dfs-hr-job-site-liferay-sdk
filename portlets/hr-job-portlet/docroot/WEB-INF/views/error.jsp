<%@ include file="init.jsp"%>
<%@page import="java.io.PrintWriter"%>
<portlet:defineObjects />
<%
	String portletTitle = PortalUtil.getPortletTitle(renderResponse);
	if (portletTitle == null) {
		portletTitle = LanguageUtil.get(pageContext, "portlet");
	}
	Throwable t = (Throwable) request.getAttribute("exception");
%>

<%-- <span class="header-back-to" > 
	<a href="#" id="<portlet:namespace/>back">�Back</a>
</span> --%>

<div class="portlet-msg-error">
	<%=LanguageUtil.format(pageContext, "is-temporarily-unavailable", portletTitle, false)%>
</div>
<p>
	<liferay-ui:message key="error-cause" />
	<%=t.getMessage()%>
</p>
<liferay-ui:toggle-area id="toggle_area" showMessage='<%=LanguageUtil.get(pageContext, "Show-stack-trace")%>'
	hideMessage='<%=LanguageUtil.get(pageContext, "Hide-stack-trace")%>' hideImage="true" defaultShowContent="false">
	<div class="toggle_area">
		<br />
		<textarea style="width: 100%; height: 300px;" readonly="yes" wrap="off">  
 <%
   	if (t != null) {
   			t.printStackTrace(new PrintWriter(out));
   		}
   %>  
           </textarea>
	</div>
</liferay-ui:toggle-area>
<br />
<hr />
<%-- back url not working
<script type="text/javascript">
<!--
var backURL='<%=ParamUtil.getString(renderRequest, "backURL")%>';
jQuery(function($){
	$('#<portlet:namespace/>back').click(function(e){
		if(typeof backURL=='undefined' || backURL || (backURL.replace(/\s/g,"") == ""))
		{
			history.back();
			console.log(window.location);
			window.location.reload();
		}
		else
		{
			window.location.replace(backURL);
		}
	});
});
//-->
</script>
 --%>