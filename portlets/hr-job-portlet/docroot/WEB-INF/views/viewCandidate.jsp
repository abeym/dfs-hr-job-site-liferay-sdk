<%@ include file="init.jsp"%>
<%!private static final Logger _log = LoggerFactory.getLogger("gov.ny.dfs.hr.view.viewPosting_jsp");%>

<%
	long candidateId = ParamUtil.getLong(request, "candidateId");
	Candidate candidate = CandidateLocalServiceUtil.getCandidate(candidateId);
%>
<liferay-portlet:renderURL var="listCandidatesURL">
	<liferay-portlet:param name="path" value="listCandidates" />
	<liferay-portlet:param name="postingId" value="<%=String.valueOf(candidate.getPostingId())%>" />
</liferay-portlet:renderURL>

<liferay-ui:header title="candidate-view" backURL="<%=listCandidatesURL%>" />

<table class="viewCandidate" style="width: 90%">
	<tr>
		<td><liferay-ui:message key="candidate-postingId" /></td>
		<td><%=candidate.getPostingId()%></td>
	</tr>
	<tr>
		<td><liferay-ui:message key="candidate-candidateId" /></td>
		<td><%=candidate.getCandidateId()%></td>
	</tr>
	<tr>
		<td><liferay-ui:message key="candidate-confirmationNum" /></td>
		<td><%=candidate.getConfirmationNum()%></td>
	</tr>
	<tr>
		<td><liferay-ui:message key="candidate-submitDate" /></td>
		<td><%=candidate.getSubmitDate()%></td>
	</tr>
	<tr>
		<td><liferay-ui:message key="candidate-name" /></td>
		<td><%=candidate.getName()%></td>
	</tr>
	<tr>
		<td><liferay-ui:message key="candidate-email" /></td>
		<td><%=candidate.getEmail()%></td>
	</tr>
	<tr>
		<td><liferay-ui:message key="candidate-phoneNum" /></td>
		<td><%=candidate.getPhoneNum()%></td>
	</tr>
	<tr>
		<td><liferay-ui:message key="candidate-qualify" /></td>
		<td><%=HRUtil.getAcceptValues(candidate.getQualify(), renderRequest)%></td>
	</tr>
	<tr>
		<td><liferay-ui:message key="candidate-qualifyDate" /></td>
		<td><%=candidate.getQualifyDate()==null?"":HRUtil.format(candidate.getQualifyDate())%></td>
	</tr>
	<tr>
		<td><liferay-ui:message key="candidate-shortlist" /></td>
		<td><%=HRUtil.getAcceptValues(candidate.getShortlist(), renderRequest)%></td>
	</tr>
	<tr>
		<td><liferay-ui:message key="candidate-shortlistDate" /></td>
		<td><%=candidate.getShortlistDate()==null?"":HRUtil.format(candidate.getShortlistDate())%></td>
	</tr>
	<tr>
		<td><liferay-ui:message key="candidate-interview" /></td>
		<td><%=HRUtil.getAcceptValues(candidate.getInterview(), renderRequest)%></td>
	</tr>
	<tr>
		<td><liferay-ui:message key="candidate-interviewDate" /></td>
		<td><%=candidate.getInterviewDate()==null?"":HRUtil.format(candidate.getInterviewDate())%></td>
	</tr>

	<tr>
		<td colspan="2"><hr /></td>
	</tr>
	<tr>
		<td colspan="2">
			<%
				int i = 1;
				for (String uuid : StringUtil.split(candidate.getResume())) {
					if(i>1)
					{
						%>,&nbsp;<%
					}
			%> 
				<a href='<%=themeDisplay.getPortalURL() + themeDisplay.getPathContext() + "/documents/" + themeDisplay.getScopeGroupId()
					+ StringPool.SLASH + uuid%>'>Resume<%=i++%>
				</a> 
			<%
				}
			%>
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<liferay-ui:toggle-area id="toggle_area" showMessage='<%=LanguageUtil.get(pageContext, "Show-resume-letter")%>' >
			<div class="FixedHeightContainer">
				<%=candidate.getResumeText()%>
			</div>
		</liferay-ui:toggle-area>
		</td>
	</tr>

	<tr>
		<td colspan="2"><hr /></td>
	</tr>

	<tr>
		<td colspan="2">
			<%
				i = 1;
				for (String uuid : StringUtil.split(candidate.getCoverLetterCSV())) {
					if(i>1)
					{
						%>,&nbsp;<%
					}
			%> 
				<a href='<%=themeDisplay.getPortalURL() + themeDisplay.getPathContext() + "/documents/" + themeDisplay.getScopeGroupId()
					+ StringPool.SLASH + uuid%>'>Cover<%=i++%>
				</a> 
			<%
				}
			%>
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<liferay-ui:toggle-area id="toggle_area" showMessage='<%=LanguageUtil.get(pageContext, "Show-cover-letter")%>' >
			<div class="FixedHeightContainer">
				<%=candidate.getCoverLetter()%>
			</div>
		</liferay-ui:toggle-area>
		</td>
	</tr>
	<tr>
		<td colspan="2"><hr /></td>
	</tr>
</table>

