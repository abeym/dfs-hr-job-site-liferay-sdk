<%@ include file="init.jsp"%>
<%!private static final Logger _log = LoggerFactory.getLogger("gov.ny.dfs.hr.view.listCandidates_jsp");%>
<%
	String redirect = PortalUtil.getCurrentURL(renderRequest);
	long postingId = ParamUtil.getLong(request, "postingId");
	Posting posting = PostingLocalServiceUtil.getPosting(postingId);

	String orderByCol = ParamUtil.getString(renderRequest, "orderByCol");
	String orderByType = ParamUtil.getString(renderRequest, "orderByType", "asc");
 	PortletURL iteratorURL = renderResponse.createRenderURL();
 	iteratorURL.setParameter("path", "listCandidates");
 	iteratorURL.setParameter("postingId", String.valueOf(postingId));
	
 	String keywords = ParamUtil.get(renderRequest, "keywords", "");

%>

<portlet:renderURL var="renderURL" />


<portlet:actionURL var="selectShortlistURL">
	<portlet:param name="action" value="selectShortlist" />
	<portlet:param name="postingId" value="<%=String.valueOf(postingId) %>" />
	<portlet:param name="renderURL" value="<%=renderURL %>" />
</portlet:actionURL>
<portlet:actionURL var="selectInterviewURL">
	<portlet:param name="action" value="selectInterview" />
	<portlet:param name="postingId" value="<%=String.valueOf(postingId) %>" />
	<portlet:param name="renderURL" value="<%=renderURL %>" />
</portlet:actionURL>
<portlet:actionURL var="selectQualifyURL">
	<portlet:param name="action" value="selectQualify" />
	<portlet:param name="postingId" value="<%=String.valueOf(postingId) %>" />
	<portlet:param name="renderURL" value="<%=renderURL %>" />
</portlet:actionURL>
<liferay-portlet:renderURL var="viewPostingURL">
	<liferay-portlet:param name="path" value="viewPosting" />
	<liferay-portlet:param name="postingId" value="<%=String.valueOf(postingId)%>" />
</liferay-portlet:renderURL>
<liferay-portlet:actionURL var="searchCandidateURL">
	<liferay-portlet:param name="action" value="searchCandidate" />
</liferay-portlet:actionURL> 



<liferay-ui:header backURL="<%=viewPostingURL%>" title="candidates-view" />

<table class="viewPosting">
<tr>
<td colspan="2">
<aui:form action="<%=searchCandidateURL%>" method="POST" name="searchfm" >
	<aui:input name="postingId" value="<%=postingId%>" type="hidden" />
	<table style="padding-bottom: 2px; border-spacing: 5px; border-collapse: separate;">
		<tr>
			<td><div class="select2-search">
					<input id="keywords" name="keywords" value="<%= keywords %>" style="width: 250px" />
				</div>
			</td>
			<td>
				<div class="select2-search">
				<input id="search" name="search" value="Search" type="submit" style="width:50px;"/>
				</div>
			</td>
		</tr>
	</table>
</aui:form>

</td>
</tr>
<tr>
<td><%=posting.getJobId() %></td>
<td ><div class="candidate-job-title"> <%=posting.getTitle() %> </div> </td>
</tr>
</table>
<hr/>

<aui:form action="<%=selectShortlistURL%>" method="POST" name="fm">
	<%-- <liferay-ui:search-container delta="10" emptyResultsMessage="no-candidates-were-found" rowChecker="<%= new RowChecker(renderResponse) %>" 
		orderByCol="<%=orderByCol %>" orderByType="<%=orderByType %>" iteratorURL="<%=iteratorURL %>"> --%>
	<liferay-ui:search-container delta="10" emptyResultsMessage="no-candidates-were-found" 
		orderByCol="<%=orderByCol %>" orderByType="<%=orderByType %>" iteratorURL="<%=iteratorURL %>">

<%

DynamicQuery dq = DynamicQueryFactoryUtil.forClass(Candidate.class);
dq.add(RestrictionsFactoryUtil.eq("postingId", postingId)) ;
if(!"".equals(keywords.trim()))
{
	String[] keywordsStrArr = keywords.split(",");
	List<Criterion> criterions = new ArrayList<Criterion>();
	for(String keywordsStr : keywordsStrArr)
	{
		keywordsStr = keywordsStr.toLowerCase();
		criterions.add(RestrictionsFactoryUtil.disjunction().add(
				RestrictionsFactoryUtil.ilike("name", "%"+keywordsStr.trim()+"%"))
				.add(RestrictionsFactoryUtil.ilike("email", "%"+keywordsStr.trim()+"%"))
				.add(RestrictionsFactoryUtil.ilike("phoneNum", "%"+keywordsStr.trim()+"%"))
			);
	}
	Disjunction dj = RestrictionsFactoryUtil.disjunction();
	for(Criterion c : criterions)
	{
		dj.add(c);
	}
	dq.add(dj) ;
}
List<Candidate> candidates = CandidateLocalServiceUtil.findWithDynamicQuery(dq, searchContainer.getStart(), searchContainer.getEnd());
int totalCandidates = (int)CandidateLocalServiceUtil.dynamicQueryCount(dq);

//sorting
BeanComparator beanComparator = new BeanComparator(orderByCol, new NullComparator(false));
List<Candidate> sorted_candidates = new ArrayList<Candidate>(candidates);
if(orderByType.equals("desc"))
{
	Collections.sort(sorted_candidates,Collections.reverseOrder(beanComparator));
	orderByType="asc";
}
else
{
	Collections.sort(sorted_candidates);
	orderByType="desc";
}

boolean isHR = HRUtil.isUserInUserGroup(user, PortletProps.get("gov.ny.dfs.hr.usergroup.hr"));
boolean isLine = HRUtil.isUserInUserGroup(user, PortletProps.get("gov.ny.dfs.hr.usergroup.line"));


%>
		<liferay-ui:search-container-results results="<%=sorted_candidates%>"
			total="<%=totalCandidates%>" />
		<liferay-ui:search-container-row className="gov.ny.dfs.hr.service.model.Candidate" keyProperty="candidateId" modelVar="candidate"
			escapedModel="<%=true%>">
			<%
				String qualify 				= (null==candidate.getQualify())?"":candidate.getQualify();
				boolean qualifyDisabled 	= ! isHR ;
				String shortlist 			= (null==candidate.getShortlist())?"":candidate.getShortlist();
				boolean shortlistDisabled 	= !(isLine && "Y".equals(candidate.getQualify()));
				String interview 			= (null==candidate.getInterview())?"":candidate.getInterview();
				boolean interviewDisabled 	= !(isHR && "Y".equals(candidate.getShortlist()) && "Y".equals(candidate.getQualify()));
			%>
			<liferay-ui:search-container-column-text name="candidate-submitDate" value="<%=HRUtil.format(candidate.getSubmitDate())%>" orderable="true" orderableProperty="submitDate" />

			<liferay-ui:search-container-column-text name="candidate-qualify" orderable="true" orderableProperty="qualify" >
				<aui:select name="qualifyArr" disabled="<%=qualifyDisabled %>" label="" inlineLabel="true" >
					<aui:option label="candidate-noaction" disabled="true" selected="<%=\"\".equals(qualify) %>" value="<%=candidate.getCandidateId()%>" />
					<aui:option label="candidate-accept" selected="<%=\"Y\".equals(qualify) %>" value="<%=\"Y\"+candidate.getCandidateId()%>" />
					<aui:option label="candidate-reject" selected="<%=\"N\".equals(qualify) %>" value="<%=\"N\"+candidate.getCandidateId()%>" />
				</aui:select>
			</liferay-ui:search-container-column-text>

			<liferay-ui:search-container-column-text name="candidate-shortlist" orderable="true" orderableProperty="shortlist" >
				<aui:select name="shortlistArr" disabled="<%=shortlistDisabled %>" label="" inlineLabel="true" >
					<aui:option label="candidate-noaction" disabled="true" selected="<%=\"\".equals(shortlist) %>" value="<%=candidate.getCandidateId()%>" />
					<aui:option label="candidate-accept" selected="<%=\"Y\".equals(shortlist) %>" value="<%=\"Y\"+candidate.getCandidateId()%>" />
					<aui:option label="candidate-reject" selected="<%=\"N\".equals(shortlist) %>" value="<%=\"N\"+candidate.getCandidateId()%>" />
				</aui:select>
			</liferay-ui:search-container-column-text>

			<liferay-ui:search-container-column-text name="candidate-interview" orderable="true" orderableProperty="interview" >
				<aui:select name="interviewArr" disabled="<%=interviewDisabled %>" label="" inlineLabel="true" onChange='<%="enableInterviewDate(this.value)"%>' >
					<aui:option label="candidate-noaction" disabled="true" selected="<%=\"\".equals(interview) %>" value="<%=candidate.getCandidateId()%>" />
					<aui:option label="candidate-accept" selected="<%=\"Y\".equals(interview) %>" value="<%=\"Y\"+candidate.getCandidateId()%>" />
					<aui:option label="candidate-reject" selected="<%=\"N\".equals(interview) %>" value="<%=\"N\"+candidate.getCandidateId()%>" />
				</aui:select>
			</liferay-ui:search-container-column-text>

			<liferay-ui:search-container-column-text name="candidate-name" orderable="true" orderableProperty="name" >
				<portlet:renderURL var="viewCandidateURL" >
					<portlet:param name="path" value="viewCandidate"/>
					<portlet:param name="candidateId" value="<%=String.valueOf(candidate.getCandidateId()) %>"/>
				</portlet:renderURL>
				<a href="<%=viewCandidateURL%>" ><%=candidate.getName()%></a>
			</liferay-ui:search-container-column-text>

			<liferay-ui:search-container-column-text name="candidate-email" value="<%=candidate.getEmail()%>" orderable="true" orderableProperty="email" />

			<liferay-ui:search-container-column-text name="candidate-phoneNum" value="<%=candidate.getPhoneNum()%>" orderable="true" orderableProperty="phoneNum" />

			<liferay-ui:search-container-column-text name="candidate-resume" >
				<%
					int i = 1;
					for (String uuid : StringUtil.split(candidate.getResume())) {
						if(i==4)
						{
							%><br/><%
						}
				%>
				<a href='<%=themeDisplay.getPortalURL() + themeDisplay.getPathContext() + "/documents/"
										+ themeDisplay.getScopeGroupId() + StringPool.SLASH + uuid %>' >Resume <%=i++%></a>
				
				<%
					}
				%>
			</liferay-ui:search-container-column-text>

			<liferay-ui:search-container-column-text name="candidate-qualifyDate" value="<%=HRUtil.format(candidate.getQualifyDate())%>" orderable="true" orderableProperty="qualifyDate" />

			<liferay-ui:search-container-column-text name="candidate-shortlistDate" value="<%=HRUtil.format(candidate.getShortlistDate())%>" orderable="true" orderableProperty="shortlistDate" />

			<liferay-ui:search-container-column-text name="candidate-interviewDate" orderable="true" orderableProperty="interviewDate" >
				<aui:input name='<%="interviewDate"+candidate.getCandidateId()%>' value="<%=HRUtil.format(candidate.getInterviewDate(), HRUtil.DATE_TIME_FORMAT)%>"
					disabled="<%=(interviewDisabled || !\"Y\".equals(interview)) %>" readonly="true" label="" inlineLabel="true" size="11"/>
			</liferay-ui:search-container-column-text>

		</liferay-ui:search-container-row>

		<liferay-ui:search-iterator paginate="true" />
	</liferay-ui:search-container>

	<aui:button-row>
		<liferay-portlet:icon-print />
		<%
			if (HRUtil.isUserInUserGroup(user, PortletProps.get("gov.ny.dfs.hr.usergroup.hr")))
			{
		%>
			<aui:button type="submit" value="candidate-qualify" onClick="submitQualify()"/>
		<%
			}
		%>
		<%
			if (HRUtil.isUserInUserGroup(user, PortletProps.get("gov.ny.dfs.hr.usergroup.line")))
			{
		%>
			<aui:button type="submit" value="candidate-shortlist" onClick="submitShortlist()"/>
		<%
			}
		%>
		<%
			if (HRUtil.isUserInUserGroup(user, PortletProps.get("gov.ny.dfs.hr.usergroup.hr")))
			{
		%>
			<%-- <aui:button type="submit" value="candidate-interview" onClick="submitInterview()"/> --%>
			<aui:button type="submit" value="candidate-interview" onClick="showPopupInterviewPanelURL()"/>
		<%
			}
		%>
		<aui:button onClick="<%=renderURL%>" type="cancel" />
	</aui:button-row>


</aui:form>

<portlet:renderURL var="popupInterviewPanelURL" windowState="<%= LiferayWindowState.EXCLUSIVE.toString() %>">
  <portlet:param name="path" value="iteratorURL.jsp"/>
  <portlet:param name="redirect" value="<%=String.valueOf(iteratorURL)%>"/>
</portlet:renderURL> 

<script type="text/javascript">

function showPopupInterviewPanelURL() {
	debugger;
	AUI().use('aui-dialog', 'aui-io', 'event', 'event-custom', function(A) {
		debugger;
		var dialog = new A.Dialog({
			title: 'Interview Panel',
			centered: true,
			draggable: true,
			modal: true
		}).plug(A.Plugin.IO, {uri: '<%=popupInterviewPanelURL%>'}).render();
		debugger;
		dialog.show();
	});
} 


function submitQualify()
{
	document.<portlet:namespace />fm.action="<%=selectQualifyURL%>";
	document.<portlet:namespace />fm.submit();
}
function submitShortlist()
{
	document.<portlet:namespace />fm.action="<%=selectShortlistURL%>";
	document.<portlet:namespace />fm.submit();
}
function submitInterview()
{
	document.<portlet:namespace />fm.action='<%=selectInterviewURL%>'+'&<portlet:namespace/>interviewDateJson='+convertToJSON('#<portlet:namespace />fm');
	 /* var interviewDateJson = $("<input>").attr("id","<portlet:namespace />interviewDateJson").attr("type", "hidden")
		.val(convertToJSON("#<portlet:namespace />fm")).appendTo('#<portlet:namespace />fm'); */ 
	document.<portlet:namespace />fm.submit(); 
	<%-- 
	$.ajax({
		type: "POST",
		url: "<%=selectInterviewURL%>",
		data: "interviewDateJson="+convertToJSON("#<portlet:namespace />fm"),
		dataType: "json"
	})
	.done(function(){
		console.log("success");
	})
	.fail(function() {
		console.log( "error" );
	})
	; --%>
}

function enableInterviewDate(val)
{
	dis = ('Y' != val.substring(0,1));
	jQuery("#<portlet:namespace />interviewDate"+val.substring(1)).prop('disabled', dis);
}

jQuery(document).ready(function () {
	$( "input[id^='<portlet:namespace />interviewDate']" ).datetimepicker();
});


function convertToJSON(elem){
    var array = jQuery(elem).serializeArray();
    var json = {};
    jQuery.each(array, function() {
    	if(this.name.indexOf('<portlet:namespace />interviewDate') == 0)
   		{
            json[this.name] = this.value || '';
   		}
    });
    return JSON.stringify(json);
}

</script>

