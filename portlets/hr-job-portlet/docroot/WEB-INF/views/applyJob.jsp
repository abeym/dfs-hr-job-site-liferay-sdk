<%@ include file="init.jsp"%>
<%!private static final Logger _log = LoggerFactory.getLogger("gov.nys.hr.jobapp.liferay.view.job_jsp");%>

<liferay-portlet:actionURL varImpl="actionURL">
	<liferay-portlet:param name="action" value="submitJob" />
</liferay-portlet:actionURL>

<aui:form action="<%=actionURL%>" method="POST" id="fm" name="fm" >
	<liferay-portlet:actionURL varImpl="applyJobURL" >
		<liferay-portlet:param name="action" value="submitJob"/>
	</liferay-portlet:actionURL>

        <div class="form-header-group">
          <h2 class="form-header" id="header_293">
            Employment Application
          </h2>
          <div class="form-subHeader" id="subHeader_293">
            Fill the form below accurately indicating your potentials and suitability to job applying for.
          </div>
        </div>
		<aui:input name="name" label="Name" />
		<aui:input name="phoneNum" label="Phone Number" />
		<aui:input name="email" label="Email" />
		<aui:input type="file" name="docAttached" label="Attach Resumes" />
		<aui:input type="textarea" name="textResume" label="Paste Your Text Resume *" inputCssClass="ta-styled"/>
		<aui:button type="submit" value="Save" />
    

</aui:form>




