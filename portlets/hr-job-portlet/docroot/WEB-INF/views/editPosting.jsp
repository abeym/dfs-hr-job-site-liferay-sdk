<%@ include file="init.jsp"%>
<%!private static final Logger _log = LoggerFactory
			.getLogger("gov.ny.dfs.hr.view.editPosting_jsp");%>
<%
	Posting posting = null;

	long postingId = ParamUtil.getLong(request, "postingId");

	if (postingId > 0) {
		posting = PostingLocalServiceUtil.getPosting(postingId);
	}

	String redirect = ParamUtil.getString(request, "redirect");
%>

<aui:model-context bean="<%=posting%>" model="<%=Posting.class%>" />
<portlet:renderURL var="renderURL" />
<portlet:actionURL var="actionURL" windowState="normal" >
	<portlet:param name="action" value='<%=((posting == null) ? "addPosting" : "updatePosting") %>'/>
</portlet:actionURL>

<liferay-ui:header backURL="<%=renderURL%>" title='<%=(posting != null) ? posting.getTitle() : "New Posting"%>' />
<%
//HRManagers
String hrManagerArr = HRUtil.getJsonHRManagers(renderRequest);
//LineManagers
String lineManagerArr = HRUtil.getJsonLineManagers(renderRequest);
%>

<jsp:include page="formError.jsp" />

<aui:form action="<%=actionURL%>" method="POST" name="fm">
	<aui:fieldset>
		<aui:input name="redirect" type="hidden" value="<%=redirect%>" />
		<aui:input name="postingId" type="hidden" value='<%=posting == null ? ParamUtil.getString(renderRequest, "postingId","") : posting.getPostingId()%>' />
		<aui:input name="jobId" value='<%=posting == null ? ParamUtil.getString(renderRequest, "jobId","") : posting.getJobId()%>'  label="posting-jobId" size="11" />
		<aui:input name="title" value='<%=posting == null ? ParamUtil.getString(renderRequest, "title","") : posting.getTitle()%>'  label="posting-title" />
		<label for="<portlet:namespace/>description" class="aui-field-label"> <liferay-ui:message key="posting-description" /> </label>
		<liferay-ui:input-editor name="description" initMethod="initEditor" editorImpl="fckeditor" />
		<br/>
		<br/>
		<label for="<portlet:namespace />openDate" class="aui-field-label"> <liferay-ui:message key="posting-openDate"/> </label>
		<input name="openDate" id="<portlet:namespace />openDate"  value="<%=posting == null ? ParamUtil.getString(renderRequest, "openDate","") : HRUtil.format(posting.getOpenDate())%>" />
		<br/>
		<br/>
		<label for="<portlet:namespace />closeDate" class="aui-field-label"> <liferay-ui:message key="posting-closeDate"/> </label>
		<input name="closeDate" id="<portlet:namespace />closeDate" value="<%=posting == null ? ParamUtil.getString(renderRequest, "closeDate","") : HRUtil.format(posting.getCloseDate())%>" />

 		<aui:input name="lineManagers" value='<%=posting == null ? ParamUtil.getString(renderRequest, "lineManagers","") : posting.getLineManagers()%>' label="posting-lineManagers"/>
		<aui:input name="hrManagers" value='<%=posting == null ? ParamUtil.getString(renderRequest, "hrManagers","") : posting.getHrManagers()%>'  label="posting-hrManagers"/>
	</aui:fieldset>

	<aui:button-row>
		<aui:button onClick="<portlet:namespace />extractFromEditor()" type="submit" />
		<aui:button onClick="<%=renderURL%>" type="cancel" />
	</aui:button-row>

</aui:form>

<script type="text/javascript">
	function <portlet:namespace />initEditor() {
		val = "<%= UnicodeFormatter.toString( (posting==null)?"<font style='font-weight: bold'>Please use this editor to enter the job posting</font>" : posting.getDescription()) %>";
		return val ;
	}
	function <portlet:namespace />extractFromEditor() {
		var x = document.<portlet:namespace />fm.<portlet:namespace />description.value = window.<portlet:namespace />editor.getHTML();
		document.<portlet:namespace />fm.submit();
	}
	jQuery(document).ready(function () {
		jQuery("#<portlet:namespace />openDate" ).datepicker();
		jQuery("#<portlet:namespace />closeDate" ).datepicker();
	});

	jQuery(document).ready(function() {
		//HR Manager
		var hrManagerArr = <%=UnicodeFormatter.toString(hrManagerArr)%>;
		$("#<portlet:namespace/>hrManagers").select2({
			placeholder: "Select the HR Manager to be notified...",
			allowClear: true,
			multiple: true,
			data:function() { 
				var myData = [];
				$.each(hrManagerArr, function(index, item){
					myData.push({id:item, text:item});
				});
				return { results: myData}; 
			}
		});
		//Line Manager
		var lineManagerArr = <%=UnicodeFormatter.toString(lineManagerArr)%>;
		$("#<portlet:namespace/>lineManagers").select2({
			placeholder: "Select the Line Manager to be notified...",
			allowClear: true,
			multiple: true,
			data:function() {
				var myData = [];
				$.each(lineManagerArr, function(index, item){
					myData.push({id:item, text:item});
				});
				return { results: myData}; 
			}
		});
	});
</script>
