<%@ include file="init.jsp"%>
<%!private static final Logger _log = LoggerFactory.getLogger("gov.ny.dfs.hr.view.viewPosting_jsp");%>

<%
	long postingId = ParamUtil.getLong(renderRequest, "postingId");
	Long postingIdStr = (Long)renderRequest.getAttribute("postingId");
	postingId = (postingIdStr!=null)?postingIdStr:postingId;
	Posting posting = PostingLocalServiceUtil.getPosting(postingId);
%>
<portlet:renderURL var="backURL" />
<liferay-portlet:renderURL var="listCandidatesURL">
	<liferay-portlet:param name="path" value="listCandidates" />
	<liferay-portlet:param name="postingId" value="<%=String.valueOf(postingId)%>" />
</liferay-portlet:renderURL>

<liferay-ui:header title="posting-view" backURL="<%=backURL%>" />
<aui:button-row>
	<aui:button onClick="<%=listCandidatesURL.toString()%>" value="candidates-view" />
</aui:button-row>

<table class="viewPosting">
<tr>
<td><liferay-ui:message key="posting-jobId" /> </td><td><%=posting.getJobId() %></td>
</tr>
<tr>
<td><liferay-ui:message key="posting-title" /> </td><td><%=posting.getTitle() %></td>
</tr>
<tr>
<td colspan="2"><hr/></td>
</tr>
<tr>
<td colspan="2"><%=posting.getDescription() %></td>
</tr>
<tr>
<td colspan="2"><hr/></td>
</tr>
<tr>
<td><liferay-ui:message key="posting-lineManagers" /> </td><td><%=posting.getLineManagers() %></td>
</tr>
<tr>
<td><liferay-ui:message key="posting-hrManagers" /> </td><td><%=posting.getHrManagers() %></td>
</tr>
<tr>
<td><liferay-ui:message key="posting-openDate" /> </td><td><%=posting.getOpenDate() %></td>
</tr>
<tr>
<td><liferay-ui:message key="posting-closeDate" /> </td><td><%=posting.getCloseDate() %></td>
</tr>
</table>

		