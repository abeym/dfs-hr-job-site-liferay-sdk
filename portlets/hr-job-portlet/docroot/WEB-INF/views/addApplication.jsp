<%@ include file="init.jsp"%>
<%!private static final Logger _log = LoggerFactory
			.getLogger("gov.ny.dfs.hr.view.addApplication_jsp");%>
<%
	long postingId = ParamUtil.getLong(request, "postingId");
	Posting posting = PostingLocalServiceUtil.getPosting(postingId);
%>

<script type="text/javascript">
var RecaptchaOptions = {
	theme : 'clean'
};
</script>
 
<portlet:renderURL var="renderURL" />

<liferay-portlet:actionURL var="actionURL" windowState="normal" doAsUserId="${doAsUserId}">
	<portlet:param name="action" value="addCandidate" />
</liferay-portlet:actionURL>

<liferay-ui:header backURL="<%=renderURL%>" title="application-apply" />

<table class="viewCandidate">
	<tr>
		<td><liferay-ui:message key="posting-jobId" /></td>
		<td><%=posting.getJobId()%></td>
	</tr>
	<tr>
		<td><liferay-ui:message key="posting-title" /></td>
		<td><%=posting.getTitle()%></td>
	</tr>
</table>

<jsp:include page="formError.jsp" />

<%-- <liferay-ui:error key="validate.application.name.required" message="validate.application.name.required" />
<liferay-ui:error key="validate.application.name.fullName" message="validate.application.name.fullName" />
<liferay-ui:error key="validate.application.email.valid" message="validate.application.email.valid" />
<liferay-ui:error key="validate.application.email.required" message="validate.application.email.required" />
<liferay-ui:error key="validate.application.email.same" message="validate.application.email.same" />
<liferay-ui:error key="validate.application.confirmEmail.required" message="validate.application.confirmEmail.required" />
<liferay-ui:error key="validate.application.resumeFile.required" message="validate.application.resumeFile.required" />
<liferay-ui:error key="validate.application.resumeFile.filesize" message="validate.application.resumeFile.filesize" />
<liferay-ui:error key="validate.application.resume.cover.required" message="validate.application.resume.cover.required" />
<liferay-ui:error exception="<%=com.liferay.portal.kernel.captcha.CaptchaException.class %>" message="validate.application.captcha.valid" /> --%>

<aui:form enctype="multipart/form-data" action="<%=actionURL%>" method="POST" name="fm" >
	<form:errors path="*" cssClass="errorblock" element="div" />
	<aui:fieldset>
		<aui:input name="postingId" type="hidden" value="<%=postingId%>">
			<%-- <aui:validator name="required" />
			<aui:validator name="alphanum" /> --%>
		</aui:input>
		<aui:input name="name" label="application-name" value='<%=ParamUtil.getString(renderRequest, "name") %>' class="cRequired" helpMessage="help.candidate.name"/>
		<form:errors path="name" cssClass="error" />
		<aui:input name="email" label="application-email" value='<%=ParamUtil.getString(renderRequest, "email") %>' class="cRequired" helpMessage="help.candidate.email"/>
		<form:errors path="email" cssClass="error" />
		<aui:input name="confirmEmail" label="application-confirmEmail" value='<%=ParamUtil.getString(renderRequest, "confirmEmail") %>' class="cRequired" helpMessage="help.candidate.confirmEmail"/>
		<form:errors path="confirmEmail" cssClass="error" />
		<aui:input name="phoneNum" label="application-phoneNum" value='<%=ParamUtil.getString(renderRequest, "phoneNum") %>' class="cRequired intlphone" minLength="7" size="15" helpMessage="help.candidate.phoneNum"/>
		<%-- <aui:input name="phoneMask" type="checkbox" label="Country"/>
		<label id="descr" for="<portlet:namespace/>phoneMask">Phone Mask</label> --%>

		<form:errors path="phoneNum" cssClass="error" />
		<table>
		<tr>
			<td class="resumeUpload">
				<aui:input name="resumeFile" label="application-resume" type="file" cssClass="multi" id="resumeFile" helpMessage="help.candidate.resumeFile"/>
				<form:errors path="resumeFile" cssClass="error" />
				<form:errors path="resumeFile" cssClass="error" />
			</td>
			<td class="resumePaste">
				<aui:input name="resumeText" type="textarea" label="application-resumeText" value='<%=ParamUtil.getString(renderRequest, "resumeText") %>' inputCssClass="ta-styled"/>
				<form:errors path="resumeText" cssClass="error" />
			</td>
		</tr>
		<tr>
			<td class="resumeUpload">
				<aui:input name="coverLetterFile" label="application-coverLetter" type="file" cssClass="multi" id="coverLetterFile" 
					helpMessage="help.candidate.coverLetterFile"/>
				<form:errors path="coverLetterFile" cssClass="error" />
			</td>
			<td class="resumePaste">
				<aui:input name="coverLetter" type="textarea" label="application-coverLetterText" value='<%=ParamUtil.getString(renderRequest, "coverLetter") %>' inputCssClass="ta-styled"/>
				<form:errors path="coverLetter" cssClass="error" />
			</td>
		</tr>
		</table>
		<!-- add captcha -->
		<liferay-portlet:resourceURL var="captchaUrl" id="captchaUrl" />
		<liferay-ui:captcha url="<%=captchaUrl%>" />
	</aui:fieldset>

	<aui:button-row>
		<aui:button type="submit" value="Submit" />
		<aui:button onClick="<%=renderURL%>" type="cancel" />
	</aui:button-row>

</aui:form>
<hr />
<div class="applicationFootNote">
	<liferay-ui:message key="application-footNote" />
</div>


<script type="text/javascript">



	jQuery(function(){ /*allow upto 5 multiple resume files*/
		jQuery('#<portlet:namespace/>resumeFile').MultiFile({
			max: 5,
			STRING: {
				remove: '<img src="<%=renderRequest.getContextPath()%>/images/delete.png" height="16" width="16" alt="x" />'
			}
		});
	});
	 
	/*submit form with files uploaded*/
	function uploadNewFile()
	{
		jQuery('#<portlet:namespace/>fm').find('input[type="file"]').each(
		function() {
			var id = jQuery(this).attr('id');
			jQuery(this).attr('name',id);
		});
		$('#<portlet:namespace/>phoneNum').val($('#<portlet:namespace/>phoneNum').val().replace(/\D/g,''));
		document.<portlet:namespace />fm.submit();
	}
	
	//custom validation rule - full name min 2 chars and a space
	$.validator.addMethod("fullName", 
		function(value, element, params) {
			var fNameRegex = /^[a-zA-Z0-9\.,]+([\s|,][a-zA-Z0-9\.,]+)+$/;
			return this.optional(element) || fNameRegex.test(value);
		}, 
		'<liferay-ui:message key="validate.application.name.fullName" />'
	);

	//custom validation rule - filesize should be less than 5MB
	$.validator.addMethod('filesize', function(value, element, param) {
		return this.optional(element) || (element.files[0].size <= param.filesize);
	});
	
	//custom validation rule - phone number should be 7 to 15 numbers starting with +  and can have space, brackets, dashes or dots.
	$.validator.addMethod('intlphone', 
		function (value) {
			var numbers = value.split(/\d/).length - 1;
			return (7 <= numbers && numbers <= 15 && value.match(/^\+?[\d \(\)-.]+[\d]+$/));
		},
		'<liferay-ui:message key="err.candidate.phoneNum.notValid" />'
	);

	/*stop form submit if not valid*/
    $("#<portlet:namespace/>fm").submit(function(e) {
		//Prevent the submit event and remain on the screen
		e.preventDefault();
		if (jQuery('#<portlet:namespace/>fm').valid())
		{
			console.log('preventDefault valid');
			return true;
		}
		else {
			console.log('preventDefault invalid');
			return false;
		}
    });

	/* jQuery('#<portlet:namespace/>fm').submit( function(e) {
		e.preventDefault();
		return false;
	}); */ 
	
	jQuery('#<portlet:namespace/>fm').validate({
		rules: {
			'<portlet:namespace/>name': {
				required: true,
				rangelength: [2, 40],
				fullName: true
			},
			'<portlet:namespace/>email': {
				required: true,
				email: true
			},
			'<portlet:namespace/>confirmEmail': {
				required: true,
				email: true,
				equalTo: '#<portlet:namespace/>email'
			},
			'<portlet:namespace/>phoneNum': {
				required: true
			},
			
			'<portlet:namespace/>resumeFile':{
				filesize: {
					required:true,
					filesize: <%=PortletProps.get("gov.ny.dfs.hr.application.filesize")%>
				}
			}
		},
		// Specify the validation error messages
        messages: {
        	'<portlet:namespace/>name': {
        		required: '<liferay-ui:message key="validate.application.name.required" />'
        	},
        	'<portlet:namespace/>email': {
				email: '<liferay-ui:message key="validate.application.email.valid" />',
				required: '<liferay-ui:message key="validate.application.email.required" />'
        	},
        	'<portlet:namespace/>confirmEmail':{
				email: '<liferay-ui:message key="validate.application.email.valid" />',
				equalTo: '<liferay-ui:message key="validate.application.email.same" />',
				required: '<liferay-ui:message key="validate.application.confirmEmail.required" />'
        	},
        	'<portlet:namespace/>phoneNum':{
				required: '<liferay-ui:message key="validate.application.phoneNum.required" />'
        	},
			'<portlet:namespace/>resumeFile':{
				filesize: {
					required:'<liferay-ui:message key="validate.application.resumeFile.required" />',
					filesize: '<liferay-ui:message key="validate.application.resumeFile.filesize" />'
				}
			}
		},
        //
        submitHandler: function(form) {
        	uploadNewFile();
        },
	});

	 
</script>


