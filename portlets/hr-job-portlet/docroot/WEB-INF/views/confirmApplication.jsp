<%@ include file="init.jsp"%>
<%!private static final Logger _log = LoggerFactory.getLogger("gov.nys.hr.jobapp.liferay.view.job_jsp");%>


<portlet:actionURL var="actionURL" windowState="normal" >
	<liferay-portlet:param name="action" value="confirmCandidate" />
</portlet:actionURL>


<aui:form action="<%=actionURL%>" method="POST" name="fm">


	<div class="form-header-group">
		<h2><liferay-ui:message key="application-thanks" /></h2>
		<div class="form-subHeader" id="subHeader_293">
			<liferay-ui:message key="application-confirmation" />
		</div>
		<br/>
		<br/>
		<aui:button type="submit" value="Back To Open Postions" />
	</div>


</aui:form>

		