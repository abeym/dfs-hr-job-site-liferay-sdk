<%@ include file="init.jsp"%>
<portlet:defineObjects />
<%
	String portletTitle = PortalUtil.getPortletTitle(renderResponse);
	if (portletTitle == null) {
		portletTitle = LanguageUtil.get(pageContext, "portlet");
	}
	Throwable t = (Throwable) request.getAttribute("exception");
	Set<String> keySet = SessionErrors.keySet(renderRequest);
	for(String key: keySet)
	{
		%>
		<liferay-ui:error key="<%=key %>" message="<%=key %>" />
		<%
	}
%>

<br />
<hr />
