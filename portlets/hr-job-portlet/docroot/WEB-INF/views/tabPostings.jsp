<%@ include file="init.jsp"%>
<%!private static final Logger _log = LoggerFactory.getLogger("gov.ny.dfs.hr.view.tabPositings_jsp");%>
<%
	String tabs1 = (String)HRUtil.getParamsValues(renderRequest, "tabs1");
	PortletURL portletURL = renderResponse.createRenderURL();
	portletURL.setWindowState(WindowState.NORMAL);
	portletURL.setParameter("tabs1", tabs1);
	String tabNames = "Postings,Candidates";
%>
<liferay-ui:header title="posting-view" />

<liferay-ui:tabs names="<%=tabNames%>" url="<%=portletURL.toString()%>" value="Posting">
	<c:if test='<%=tabs1.equals("Postings")%>'>
		<liferay-util:include page="/WEB-INF/views/listPostings.jsp" servletContext="<%=this.getServletContext()%>" />
	</c:if>
	<c:if test='<%=tabs1.equals("Candidates")%>'>
		<liferay-util:include page="/WEB-INF/views/listCandidates.jsp" servletContext="<%=this.getServletContext()%>" />
	</c:if>
</liferay-ui:tabs>
