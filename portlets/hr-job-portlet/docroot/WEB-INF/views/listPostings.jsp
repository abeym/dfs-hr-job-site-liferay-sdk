<%@ include file="init.jsp"%>
<%!private static final Logger _log = LoggerFactory.getLogger("gov.ny.dfs.hr.view.listPostings_jsp");%>

<liferay-ui:header title="posting-view" />

<liferay-portlet:renderURL var="renderURL">
	<liferay-portlet:param name="path" value="editPosting" />
</liferay-portlet:renderURL>

<liferay-portlet:actionURL var="searchPostingURL">
	<liferay-portlet:param name="action" value="searchPosting" />
</liferay-portlet:actionURL> 

<%
 	PortletURL iteratorURL = renderResponse.createRenderURL();
 	/* 
 	PortletURL iteratorURL = renderResponse.createRenderURL();
 	iteratorURL.setParameter("parameterName", "posting-jobId");
 	iteratorURL.setParameter("parameterName", "parameterValue");
 	 */
 	 boolean isHR = false;
 	//job title array
 	String keywordsArr = HRUtil.getJsonJobKeywords();
 	String keywords = ParamUtil.get(renderRequest, "keywords", "");
	String orderByCol = ParamUtil.getString(renderRequest, "orderByCol");
	String orderByType = ParamUtil.getString(renderRequest, "orderByType", "asc");

 %>
<aui:form action="<%=searchPostingURL%>" method="POST" name="fm" >
	<table style="padding-bottom: 2px; border-spacing: 5px; border-collapse: separate;">
		<tr>
			<td><div class="select2-search">
					<input id="keywords" name="keywords" value="<%= keywords %>" style="width: 250px" />
				</div>
			</td>
			<td>
				<div class="select2-search">
				<input id="search" name="search" value="Search" type="submit" style="width:50px;"/>
				</div>
			</td>
		</tr>
	</table>
</aui:form>

<liferay-ui:search-container delta="10" emptyResultsMessage="no-postings-were-found" iteratorURL="<%=iteratorURL%>" 
	orderByCol="<%=orderByCol %>" orderByType="<%=orderByType %>">
	<%

	List<Posting> postings = new ArrayList<Posting>();
	int postingsCount = 0;
	DynamicQuery dq = DynamicQueryFactoryUtil.forClass(Posting.class);

	if(!"".equals(keywords.trim()))
	{
		String[] keywordsStrArr = keywords.split(",");
		List<Criterion> criterions = new ArrayList<Criterion>();
		for(String keywordsStr : keywordsStrArr)
		{
			keywordsStr = keywordsStr.toLowerCase();
			criterions.add(RestrictionsFactoryUtil.or(
					RestrictionsFactoryUtil.ilike("jobId", "%"+keywordsStr.trim()+"%"), 
					RestrictionsFactoryUtil.ilike("title", "%"+keywordsStr.trim()+"%"))
				);
		}
		Disjunction dj = RestrictionsFactoryUtil.disjunction();
		for(Criterion c : criterions)
		{
			dj.add(c);
		}
		dq.add(dj);
	}
	
	if (HRUtil.isUserInUserGroup(user, PortletProps.get("gov.ny.dfs.hr.usergroup.hr")) || renderRequest.isUserInRole("administrator"))
	{
		/* postings = PostingLocalServiceUtil.getPostings(searchContainer.getStart(), searchContainer.getEnd());
		postingsCount = PostingLocalServiceUtil.getPostingsCount(); */
		isHR = true;
	}
	else if (HRUtil.isUserInUserGroup(user, PortletProps.get("gov.ny.dfs.hr.usergroup.line")))
	{
		String mgrEmail = user.getEmailAddress();
		dq.add(RestrictionsFactoryUtil.ilike("lineManagers", "%"+mgrEmail+"%"));
	}

	postings = PostingLocalServiceUtil.findWithDynamicQuery(dq, searchContainer.getStart(), searchContainer.getEnd());
	postingsCount = (int)PostingLocalServiceUtil.dynamicQueryCount(dq);
	/* searchContainer.setResults(postings);
	searchContainer.setTotal(postingsCount); */
	/* searchContainer.setDelta(10);
	searchContainer.setIteratorURL(iteratorURL); */
	/* searchContainer.setOrderByCol(orderByCol);
	searchContainer.setOrderByType(orderByType); */

	pageContext.setAttribute("isHR", String.valueOf(isHR));
	
	//sorting
	BeanComparator beanComparator = new BeanComparator(orderByCol, new NullComparator(false));
	List<Posting> copy_postings = new ArrayList<Posting>(postings);
	if(orderByType.equals("desc"))
	{
		Collections.sort(copy_postings,Collections.reverseOrder(beanComparator));
		orderByType="asc";
	}
	else
	{
		Collections.sort(copy_postings);
		orderByType="desc";
	}
	
%>


	<liferay-ui:search-container-results results="<%=copy_postings%>"
		total="<%=postingsCount%>" />


	<liferay-ui:search-container-row className="gov.ny.dfs.hr.service.model.Posting" keyProperty="postingId" modelVar="posting" escapedModel="<%=true%>">
		<liferay-ui:search-container-column-text name="posting-jobId" value="<%=posting.getJobId()%>" orderable="true" orderableProperty="jobId" />

		<liferay-ui:search-container-column-text name="posting-title" orderable="true" orderableProperty="title" >
			<liferay-portlet:renderURL var="listCandidatesURL">
				<liferay-portlet:param name="path" value="listCandidates" />
				<liferay-portlet:param name="postingId" value="<%=String.valueOf(posting.getPostingId())%>" />
			</liferay-portlet:renderURL>
			<a href="<%=listCandidatesURL%>"> <%=posting.getTitle()%></a>
		</liferay-ui:search-container-column-text>

		<liferay-ui:search-container-column-text name="posting-openDate" value="<%=HRUtil.format(posting.getOpenDate())%>" orderable="true"
			orderableProperty="openDate" />

		<liferay-ui:search-container-column-text name="posting-closeDate" value="<%=HRUtil.format(posting.getCloseDate())%>" orderable="true"
			orderableProperty="closeDate" />

		<%
			if(isHR){
			%>
		<liferay-ui:search-container-column-jsp align="right" path="/WEB-INF/views/actionPositings.jsp" />
		<% 
			}
			%>

	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator paginate="true" />
</liferay-ui:search-container>

<%
if(isHR){
%>
<aui:button-row>
	<aui:button onClick="<%=renderURL.toString()%>" value="posting-add" />
</aui:button-row>
<% 
}
%>

<portlet:actionURL name="deletePosting" var="deleteURL" >
	<portlet:param name="action" value="deletePosting" />
</portlet:actionURL>



<script type="text/javascript">
$(function() {
	
	/*job title*/
	var keywordsArr = <%=keywordsArr %>;
	$("#keywords").select2({
	    placeholder: "Job Id, Title...",
	    allowClear: true,
	    multiple: true,
	    data:function() { 
	        var myData = [];
	        $.each(keywordsArr, function(index, item){
	            myData.push({id:item, text:item})
	        });
	        return { results: myData}; 
	    }
	});

});

function <portlet:namespace />deletePosting(id) {
	if (confirm('<%= UnicodeLanguageUtil.get(pageContext, "are-you-sure-you-want-to-delete-the-posting") %>')) {
		var action = '<%=deleteURL%>' + '&<portlet:namespace/>postingId='+id;
		jQuery('<form />').attr('action', action).submit();
	}
}
</script>

