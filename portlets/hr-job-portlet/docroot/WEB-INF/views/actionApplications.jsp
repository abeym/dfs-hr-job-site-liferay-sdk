<%@ include file="init.jsp"%>
<%!private static final Logger _log = LoggerFactory.getLogger("gov.ny.dfs.hr.view.actionPositings_jsp");%>


<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import="com.liferay.portal.util.PortalUtil"%>

<portlet:defineObjects />

<%
	ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	Posting posting = (Posting) row.getObject();

	String name = Posting.class.getName();
	long postingId = posting.getPostingId();

	String redirect = PortalUtil.getCurrentURL(renderRequest);
%>

<liferay-ui:icon-menu>

	<portlet:renderURL var="addCandidateURL">
		<portlet:param name="path" value="addApplication" />
		<portlet:param name="postingId" value="<%=String.valueOf(postingId)%>" />
		<portlet:param name="redirect" value="<%=redirect%>" />
	</portlet:renderURL>
	<liferay-ui:icon image="add" url="<%=addCandidateURL.toString()%>" label="application-apply"/>

</liferay-ui:icon-menu>
