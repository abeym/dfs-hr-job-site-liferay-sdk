<%@ include file="init.jsp"%>
<%!private static final Logger _log = LoggerFactory.getLogger("gov.ny.dfs.hr.view.editCandidate_jsp");%>
<%
	Candidate candidate = null;

	long candidateId = ParamUtil.getLong(request, "candidateId");

	if (candidateId > 0) {
		candidate = CandidateLocalServiceUtil.getCandidate(candidateId);
	}

	String redirect = ParamUtil.getString(request, "redirect");
%>

<aui:model-context bean="<%=candidate%>" model="<%=Candidate.class%>" />
<portlet:renderURL var="renderURL" />
<portlet:actionURL name='<%=candidate == null ? "add" : "update"%>' var="actionURL" windowState="normal" />

<liferay-ui:header backURL="<%=renderURL%>" title='<%=(candidate != null) ? candidate.getName() : "New Candidate"%>' />

<aui:form action="<%=actionURL%>" method="POST" name="fm">
	<aui:fieldset>
		<aui:input name="redirect" type="hidden" value="<%=redirect%>" />
		<aui:input name="candidateId" type="hidden" value='<%=candidate == null ? "" : candidate.getCandidateId()%>' />
		<aui:input name="confirmationNum" />
		<aui:input name="submitDate" />
		<aui:input name="name" />
		<aui:input name="email" />
		<aui:input name="confirmEmail" />
		<aui:input name="phoneNum" />
		<aui:input name="resume" />
		<aui:input name="coverLetter" />
		<liferay-ui:input-checkbox param="shortlist" />
		<liferay-ui:input-date name="shortlistDate" />
		<liferay-ui:input-checkbox param="interview" />
		<liferay-ui:input-date yearRangeEnd="" yearRangeStart="" dayParam="interviewDate" monthAndYearNullable="flase" />

	</aui:fieldset>

	<!-- add captcha -->
	<liferay-portlet:resourceURL var="captchaUrl" id="captchaUrl" />
	<liferay-ui:captcha url="captchaUrl" />


	<aui:button-row>
		<aui:button type="submit" />
		<aui:button onClick="<%=renderURL%>" type="cancel" />
	</aui:button-row>

</aui:form>