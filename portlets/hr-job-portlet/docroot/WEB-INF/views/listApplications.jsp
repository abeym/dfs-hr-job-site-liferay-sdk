<%@page import="com.liferay.portal.kernel.dao.orm.Disjunction"%>
<%@ include file="init.jsp"%>
<%!private static final Logger _log = LoggerFactory.getLogger("gov.ny.dfs.hr.view.listPositings_jsp");%>
<liferay-ui:header title="positions-view" />
<%
//job title array
String keywordsArr = HRUtil.getJsonJobKeywords();
String keywords = ParamUtil.get(renderRequest, "keywords", "");
PortletURL iteratorURL = renderResponse.createRenderURL();
/* iteratorURL.setParameter("parameterName", "posting-jobId");
iteratorURL.setParameter("parameterName", "parameterValue"); */
String orderByCol = ParamUtil.getString(renderRequest, "orderByCol");
String orderByType = ParamUtil.getString(renderRequest, "orderByType", "asc");
%>

<liferay-ui:search-container delta="10" emptyResultsMessage="no-positons-were-found" iteratorURL="<%=iteratorURL %>" orderByCol="<%=orderByCol %>" orderByType="<%=orderByType %>" >

<%
List<Posting> postings = new ArrayList<Posting>();
int postingsCount = 0;
Date date = new Date();
DynamicQuery dq = DynamicQueryFactoryUtil.forClass(Posting.class);
dq.add(RestrictionsFactoryUtil.or(RestrictionsFactoryUtil.ge("closeDate", date), 
		RestrictionsFactoryUtil.isNull("closeDate")) );
dq.add(RestrictionsFactoryUtil.or(RestrictionsFactoryUtil.le("openDate", date), 
		RestrictionsFactoryUtil.isNull("openDate")) );

if(!"".equals(keywords.trim()))
{
	String[] keywordsStrArr = keywords.split(",");
	List<Criterion> criterions = new ArrayList<Criterion>();
	for(String keywordsStr : keywordsStrArr)
	{
			criterions.add(RestrictionsFactoryUtil.or(
					RestrictionsFactoryUtil.ilike("jobId", "%"+keywordsStr.trim()+"%"), 
					RestrictionsFactoryUtil.ilike("title", "%"+keywordsStr.trim()+"%"))
				);
	}
	Disjunction dj = RestrictionsFactoryUtil.disjunction();
	for(Criterion c : criterions)
	{
		dj.add(c);
	}
	dq.add(dj);
}

postings = PostingLocalServiceUtil.findWithDynamicQuery(dq, searchContainer.getStart(), searchContainer.getEnd());
postingsCount = (int)PostingLocalServiceUtil.dynamicQueryCount(dq);
/* iteratorURL.setParameter("parameterName", "posting-jobId");
iteratorURL.setParameter("parameterName", "parameterValue"); */

//sorting
BeanComparator beanComparator = new BeanComparator(orderByCol, new NullComparator(false));
List<Posting> copy_postings = new ArrayList<Posting>(postings);
if(orderByType.equals("desc"))
{
	Collections.sort(copy_postings,Collections.reverseOrder(beanComparator));
	orderByType="asc";
}
else
{
	Collections.sort(copy_postings);
	orderByType="desc";
}

%>
<liferay-portlet:actionURL var="searchPostingURL">
	<liferay-portlet:param name="action" value="searchPosting" />
</liferay-portlet:actionURL> 

<aui:form action="<%=searchPostingURL%>" method="POST" name="fm" >
	<table style="padding-bottom: 2px; border-spacing: 5px; border-collapse: separate;">
		<tr>
			<td style="padding-left: 10px;"><label for="keywords" style="width: 100px">Search: </label></td>
			<td><div class="select2-search">
					<input id="keywords" name="keywords" value="<%= keywords %>" style="width: 250px" />
				</div>
			</td>
			<td>
				<div class="select2-search">
				<input id="search" name="search" value="Search" type="submit" style="width:50px;"/>
				</div>
			</td>
		</tr>
	</table>
</aui:form>


	<liferay-ui:search-container-results results="<%=copy_postings%>" total="<%=postingsCount%>" />
	<liferay-ui:search-container-row className="gov.ny.dfs.hr.service.model.Posting" keyProperty="postingId" modelVar="posting" escapedModel="<%=true%>">

		<liferay-ui:search-container-column-text name="posting-jobId" value="<%=posting.getJobId()%>" orderable="true" orderableProperty="jobId"/>

		<liferay-ui:search-container-column-text name="posting-title" orderable="true" orderableProperty="title">
			<liferay-portlet:renderURL var="viewPostingURL">
				<liferay-portlet:param name="path" value="viewApplication" />
				<liferay-portlet:param name="postingId" value="<%=String.valueOf(posting.getPostingId())%>" />
			</liferay-portlet:renderURL>
			<a href="<%=viewPostingURL%>"> <%=posting.getTitle()%></a>
		</liferay-ui:search-container-column-text>

		<liferay-ui:search-container-column-text name="posting-openDate" value="<%=HRUtil.format(posting.getOpenDate())%>" orderable="true" orderableProperty="openDate" />

		<liferay-ui:search-container-column-text name="posting-closeDate" value="<%=HRUtil.format(posting.getCloseDate())%>" orderable="true" orderableProperty="closeDate" />

		<liferay-ui:search-container-column-text>
			<portlet:renderURL var="addApplicationURL">
				<portlet:param name="path" value="addApplication" />
				<portlet:param name="postingId" value="<%=String.valueOf(posting.getPostingId())%>" />
			</portlet:renderURL>
			<a href="<%=addApplicationURL%>"> <liferay-ui:message key="application-apply" />
			</a>
		</liferay-ui:search-container-column-text>

	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator />
</liferay-ui:search-container>
<hr/>
<div class="applicationFootNote">
	<liferay-ui:message key="application-footNote"/>
</div>

<script type="text/javascript">
$(function() {
	
	/*job title*/
	var keywordsArr = <%=keywordsArr %>;
	$("#keywords").select2({
	    placeholder: "Job Id, Title...",
	    allowClear: true,
	    multiple: true,
	    data:function() { 
	        var myData = [];
	        $.each(keywordsArr, function(index, item){
	            myData.push({id:item, text:item})
	        });
	        return { results: myData}; 
	    }
	});

});
</script>
