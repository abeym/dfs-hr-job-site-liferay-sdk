
package gov.ny.dfs.hr.search;

import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.log.*;
import com.liferay.portal.kernel.util.*;
import com.liferay.portlet.*;
import com.liferay.portlet.enterpriseadmin.util.EnterpriseAdminUtil;

import gov.ny.dfs.hr.service.model.Posting;

import java.util.*;

import javax.portlet.*;

/**
 * @author Abey
 *
 */
public class PostingSearch extends SearchContainer<Posting> {

	static List<String> headerNames = new ArrayList<String>();
	static Map<String, String> orderableHeaders = new HashMap<String, String>();

	static {
		headerNames.add("name");
		headerNames.add("type");

		orderableHeaders.put("name", "name");
		orderableHeaders.put("type", "type");
	}

	public static final String EMPTY_RESULTS_MESSAGE =
		"no-communities-were-found";

	public PostingSearch(PortletRequest portletRequest, PortletURL iteratorURL) {
		/*super(
			portletRequest, iteratorURL, headerNames, EMPTY_RESULTS_MESSAGE);*/
		super(
				portletRequest, new PostingDisplayTerms(portletRequest),
				new PostingSearchTerms(portletRequest), DEFAULT_CUR_PARAM,
				DEFAULT_DELTA, iteratorURL, headerNames, EMPTY_RESULTS_MESSAGE);


		PostingDisplayTerms displayTerms = (PostingDisplayTerms)getDisplayTerms();

		iteratorURL.setParameter(
			PostingDisplayTerms.JOB_ID, displayTerms.getJobId());
		iteratorURL.setParameter(
			PostingDisplayTerms.TITLE, displayTerms.getTitle());
		iteratorURL.setParameter(
				PostingDisplayTerms.DESCRIPTION, displayTerms.getDescription());

		try {
			PortalPreferences preferences =
				PortletPreferencesFactoryUtil.getPortalPreferences(
					portletRequest);

			String orderByCol = ParamUtil.getString(
				portletRequest, "orderByCol");
			String orderByType = ParamUtil.getString(
				portletRequest, "orderByType");

			if (Validator.isNotNull(orderByCol) &&
				Validator.isNotNull(orderByType)) {

				preferences.setValue(
						"hr-job-posting", "groups-order-by-col",
					orderByCol);
				preferences.setValue(
						"hr-job-posting", "groups-order-by-type",
					orderByType);
			}
			else {
				orderByCol = preferences.getValue(
						"hr-job-posting", "groups-order-by-col",
					"name");
				orderByType = preferences.getValue(
						"hr-job-posting", "groups-order-by-type",
					"asc");
			}

			OrderByComparator orderByComparator =
				EnterpriseAdminUtil.getGroupOrderByComparator(
					orderByCol, orderByType);

			setOrderableHeaders(orderableHeaders);
			setOrderByCol(orderByCol);
			setOrderByType(orderByType);
			setOrderByComparator(orderByComparator);
		}
		catch (Exception e) {
			_log.error(e);
		}
	}

	private static Log _log = LogFactoryUtil.getLog(PostingSearch.class);

}