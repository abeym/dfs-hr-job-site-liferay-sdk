/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package gov.ny.dfs.hr.service.service.impl;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.*;
import com.liferay.portal.service.ServiceContext;

import gov.ny.dfs.hr.service.model.Posting;
import gov.ny.dfs.hr.service.service.base.PostingLocalServiceBaseImpl;
import gov.ny.dfs.hr.service.service.persistence.*;

import java.util.List;

/**
 * The implementation of the posting local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link gov.ny.dfs.hr.service.service.PostingLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Abey
 * @see gov.ny.dfs.hr.service.service.base.PostingLocalServiceBaseImpl
 * @see gov.ny.dfs.hr.service.service.PostingLocalServiceUtil
 */
public class PostingLocalServiceImpl extends PostingLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link gov.ny.dfs.hr.service.service.PostingLocalServiceUtil} to access the posting local service.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public void clearCache(Posting posting) {
		getPersistence().clearCache(posting);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public List<Posting> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public List<Posting> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public List<Posting> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#remove(com.liferay.portal.model.BaseModel)
	 */
	public Posting remove(Posting posting) throws SystemException {
		return getPersistence().remove(posting);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public Posting update(Posting posting, boolean merge)
		throws SystemException {
		return getPersistence().update(posting, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public Posting update(Posting posting, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(posting, merge, serviceContext);
	}

	/**
	* Caches the posting in the entity cache if it is enabled.
	*
	* @param posting the posting to cache
	*/
	public void cacheResult(gov.ny.dfs.hr.service.model.Posting posting) {
		getPersistence().cacheResult(posting);
	}

	/**
	* Caches the postings in the entity cache if it is enabled.
	*
	* @param postings the postings to cache
	*/
	public void cacheResult(
		java.util.List<gov.ny.dfs.hr.service.model.Posting> postings) {
		getPersistence().cacheResult(postings);
	}

	/**
	* Creates a new posting with the primary key. Does not add the posting to the database.
	*
	* @param postingId the primary key for the new posting
	* @return the new posting
	*/
	public gov.ny.dfs.hr.service.model.Posting create(long postingId) {
		return getPersistence().create(postingId);
	}

	/**
	* Removes the posting with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param postingId the primary key of the posting to remove
	* @return the posting that was removed
	* @throws gov.ny.dfs.hr.service.NoSuchPostingException if a posting with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Posting remove(long postingId)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchPostingException {
		return getPersistence().remove(postingId);
	}

	public gov.ny.dfs.hr.service.model.Posting updateImpl(
		gov.ny.dfs.hr.service.model.Posting posting, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(posting, merge);
	}

	/**
	* Finds the posting with the primary key or throws a {@link gov.ny.dfs.hr.service.NoSuchPostingException} if it could not be found.
	*
	* @param postingId the primary key of the posting to find
	* @return the posting
	* @throws gov.ny.dfs.hr.service.NoSuchPostingException if a posting with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Posting findByPrimaryKey(
		long postingId)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchPostingException {
		return getPersistence().findByPrimaryKey(postingId);
	}

	/**
	* Finds the posting with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param postingId the primary key of the posting to find
	* @return the posting, or <code>null</code> if a posting with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Posting fetchByPrimaryKey(
		long postingId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(postingId);
	}

	/**
	* Finds all the postings where jobId = &#63;.
	*
	* @param jobId the job ID to search with
	* @return the matching postings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Posting> findByJobId(
		java.lang.String jobId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByJobId(jobId);
	}

	/**
	* Finds a range of all the postings where jobId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param jobId the job ID to search with
	* @param start the lower bound of the range of postings to return
	* @param end the upper bound of the range of postings to return (not inclusive)
	* @return the range of matching postings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Posting> findByJobId(
		java.lang.String jobId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByJobId(jobId, start, end);
	}

	/**
	* Finds an ordered range of all the postings where jobId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param jobId the job ID to search with
	* @param start the lower bound of the range of postings to return
	* @param end the upper bound of the range of postings to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching postings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Posting> findByJobId(
		java.lang.String jobId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByJobId(jobId, start, end, orderByComparator);
	}

	/**
	* Finds the first posting in the ordered set where jobId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param jobId the job ID to search with
	* @param orderByComparator the comparator to order the set by
	* @return the first matching posting
	* @throws gov.ny.dfs.hr.service.NoSuchPostingException if a matching posting could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Posting findByJobId_First(
		java.lang.String jobId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchPostingException {
		return getPersistence().findByJobId_First(jobId, orderByComparator);
	}

	/**
	* Finds the last posting in the ordered set where jobId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param jobId the job ID to search with
	* @param orderByComparator the comparator to order the set by
	* @return the last matching posting
	* @throws gov.ny.dfs.hr.service.NoSuchPostingException if a matching posting could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Posting findByJobId_Last(
		java.lang.String jobId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchPostingException {
		return getPersistence().findByJobId_Last(jobId, orderByComparator);
	}

	/**
	* Finds the postings before and after the current posting in the ordered set where jobId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param postingId the primary key of the current posting
	* @param jobId the job ID to search with
	* @param orderByComparator the comparator to order the set by
	* @return the previous, current, and next posting
	* @throws gov.ny.dfs.hr.service.NoSuchPostingException if a posting with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Posting[] findByJobId_PrevAndNext(
		long postingId, java.lang.String jobId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchPostingException {
		return getPersistence()
				   .findByJobId_PrevAndNext(postingId, jobId, orderByComparator);
	}

	/**
	* Finds all the postings where title = &#63;.
	*
	* @param title the title to search with
	* @return the matching postings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Posting> findByTitle(
		java.lang.String title)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByTitle(title);
	}

	/**
	* Finds a range of all the postings where title = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param title the title to search with
	* @param start the lower bound of the range of postings to return
	* @param end the upper bound of the range of postings to return (not inclusive)
	* @return the range of matching postings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Posting> findByTitle(
		java.lang.String title, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByTitle(title, start, end);
	}

	/**
	* Finds an ordered range of all the postings where title = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param title the title to search with
	* @param start the lower bound of the range of postings to return
	* @param end the upper bound of the range of postings to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching postings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Posting> findByTitle(
		java.lang.String title, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByTitle(title, start, end, orderByComparator);
	}

	/**
	* Finds the first posting in the ordered set where title = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param title the title to search with
	* @param orderByComparator the comparator to order the set by
	* @return the first matching posting
	* @throws gov.ny.dfs.hr.service.NoSuchPostingException if a matching posting could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Posting findByTitle_First(
		java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchPostingException {
		return getPersistence().findByTitle_First(title, orderByComparator);
	}

	/**
	* Finds the last posting in the ordered set where title = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param title the title to search with
	* @param orderByComparator the comparator to order the set by
	* @return the last matching posting
	* @throws gov.ny.dfs.hr.service.NoSuchPostingException if a matching posting could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Posting findByTitle_Last(
		java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchPostingException {
		return getPersistence().findByTitle_Last(title, orderByComparator);
	}

	/**
	* Finds the postings before and after the current posting in the ordered set where title = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param postingId the primary key of the current posting
	* @param title the title to search with
	* @param orderByComparator the comparator to order the set by
	* @return the previous, current, and next posting
	* @throws gov.ny.dfs.hr.service.NoSuchPostingException if a posting with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Posting[] findByTitle_PrevAndNext(
		long postingId, java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchPostingException {
		return getPersistence()
				   .findByTitle_PrevAndNext(postingId, title, orderByComparator);
	}

	/**
	* Finds all the postings where description = &#63;.
	*
	* @param description the description to search with
	* @return the matching postings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Posting> findByDescription(
		java.lang.String description)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByDescription(description);
	}

	/**
	* Finds a range of all the postings where description = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param description the description to search with
	* @param start the lower bound of the range of postings to return
	* @param end the upper bound of the range of postings to return (not inclusive)
	* @return the range of matching postings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Posting> findByDescription(
		java.lang.String description, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByDescription(description, start, end);
	}

	/**
	* Finds an ordered range of all the postings where description = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param description the description to search with
	* @param start the lower bound of the range of postings to return
	* @param end the upper bound of the range of postings to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching postings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Posting> findByDescription(
		java.lang.String description, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByDescription(description, start, end, orderByComparator);
	}

	/**
	* Finds the first posting in the ordered set where description = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param description the description to search with
	* @param orderByComparator the comparator to order the set by
	* @return the first matching posting
	* @throws gov.ny.dfs.hr.service.NoSuchPostingException if a matching posting could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Posting findByDescription_First(
		java.lang.String description,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchPostingException {
		return getPersistence()
				   .findByDescription_First(description, orderByComparator);
	}

	/**
	* Finds the last posting in the ordered set where description = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param description the description to search with
	* @param orderByComparator the comparator to order the set by
	* @return the last matching posting
	* @throws gov.ny.dfs.hr.service.NoSuchPostingException if a matching posting could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Posting findByDescription_Last(
		java.lang.String description,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchPostingException {
		return getPersistence()
				   .findByDescription_Last(description, orderByComparator);
	}

	/**
	* Finds the postings before and after the current posting in the ordered set where description = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param postingId the primary key of the current posting
	* @param description the description to search with
	* @param orderByComparator the comparator to order the set by
	* @return the previous, current, and next posting
	* @throws gov.ny.dfs.hr.service.NoSuchPostingException if a posting with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Posting[] findByDescription_PrevAndNext(
		long postingId, java.lang.String description,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchPostingException {
		return getPersistence()
				   .findByDescription_PrevAndNext(postingId, description,
			orderByComparator);
	}

	/**
	* Finds all the postings where jobId = &#63; and title = &#63;.
	*
	* @param jobId the job ID to search with
	* @param title the title to search with
	* @return the matching postings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Posting> findByJ_T(
		java.lang.String jobId, java.lang.String title)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByJ_T(jobId, title);
	}

	/**
	* Finds a range of all the postings where jobId = &#63; and title = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param jobId the job ID to search with
	* @param title the title to search with
	* @param start the lower bound of the range of postings to return
	* @param end the upper bound of the range of postings to return (not inclusive)
	* @return the range of matching postings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Posting> findByJ_T(
		java.lang.String jobId, java.lang.String title, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByJ_T(jobId, title, start, end);
	}

	/**
	* Finds an ordered range of all the postings where jobId = &#63; and title = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param jobId the job ID to search with
	* @param title the title to search with
	* @param start the lower bound of the range of postings to return
	* @param end the upper bound of the range of postings to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching postings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Posting> findByJ_T(
		java.lang.String jobId, java.lang.String title, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByJ_T(jobId, title, start, end, orderByComparator);
	}

	/**
	* Finds the first posting in the ordered set where jobId = &#63; and title = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param jobId the job ID to search with
	* @param title the title to search with
	* @param orderByComparator the comparator to order the set by
	* @return the first matching posting
	* @throws gov.ny.dfs.hr.service.NoSuchPostingException if a matching posting could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Posting findByJ_T_First(
		java.lang.String jobId, java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchPostingException {
		return getPersistence().findByJ_T_First(jobId, title, orderByComparator);
	}

	/**
	* Finds the last posting in the ordered set where jobId = &#63; and title = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param jobId the job ID to search with
	* @param title the title to search with
	* @param orderByComparator the comparator to order the set by
	* @return the last matching posting
	* @throws gov.ny.dfs.hr.service.NoSuchPostingException if a matching posting could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Posting findByJ_T_Last(
		java.lang.String jobId, java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchPostingException {
		return getPersistence().findByJ_T_Last(jobId, title, orderByComparator);
	}

	/**
	* Finds the postings before and after the current posting in the ordered set where jobId = &#63; and title = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param postingId the primary key of the current posting
	* @param jobId the job ID to search with
	* @param title the title to search with
	* @param orderByComparator the comparator to order the set by
	* @return the previous, current, and next posting
	* @throws gov.ny.dfs.hr.service.NoSuchPostingException if a posting with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Posting[] findByJ_T_PrevAndNext(
		long postingId, java.lang.String jobId, java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchPostingException {
		return getPersistence()
				   .findByJ_T_PrevAndNext(postingId, jobId, title,
			orderByComparator);
	}

	/**
	* Finds all the postings where jobId = &#63; and title = &#63; and description = &#63; and lineManagers = &#63; and hrManagers = &#63;.
	*
	* @param jobId the job ID to search with
	* @param title the title to search with
	* @param description the description to search with
	* @param lineManagers the line managers to search with
	* @param hrManagers the hr managers to search with
	* @return the matching postings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Posting> findByJ_T_D_L_H(
		java.lang.String jobId, java.lang.String title,
		java.lang.String description, java.lang.String lineManagers,
		java.lang.String hrManagers)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().
				findByJ_T_D_L_H(jobId, title, description, lineManagers, hrManagers);
	}

	/**
	* Finds a range of all the postings where jobId = &#63; and title = &#63; and description = &#63; and lineManagers = &#63; and hrManagers = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param jobId the job ID to search with
	* @param title the title to search with
	* @param description the description to search with
	* @param lineManagers the line managers to search with
	* @param hrManagers the hr managers to search with
	* @param start the lower bound of the range of postings to return
	* @param end the upper bound of the range of postings to return (not inclusive)
	* @return the range of matching postings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Posting> findByJ_T_D_L_H(
		java.lang.String jobId, java.lang.String title,
		java.lang.String description, java.lang.String lineManagers,
		java.lang.String hrManagers, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByJ_T_D_L_H(jobId, title, description, lineManagers,
			hrManagers, start, end);
	}

	/**
	* Finds an ordered range of all the postings where jobId = &#63; and title = &#63; and description = &#63; and lineManagers = &#63; and hrManagers = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param jobId the job ID to search with
	* @param title the title to search with
	* @param description the description to search with
	* @param lineManagers the line managers to search with
	* @param hrManagers the hr managers to search with
	* @param start the lower bound of the range of postings to return
	* @param end the upper bound of the range of postings to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching postings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Posting> findByJ_T_D_L_H(
		java.lang.String jobId, java.lang.String title,
		java.lang.String description, java.lang.String lineManagers,
		java.lang.String hrManagers, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByJ_T_D_L_H(jobId, title, description, lineManagers,
			hrManagers, start, end, orderByComparator);
	}

	/**
	* Finds the first posting in the ordered set where jobId = &#63; and title = &#63; and description = &#63; and lineManagers = &#63; and hrManagers = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param jobId the job ID to search with
	* @param title the title to search with
	* @param description the description to search with
	* @param lineManagers the line managers to search with
	* @param hrManagers the hr managers to search with
	* @param orderByComparator the comparator to order the set by
	* @return the first matching posting
	* @throws gov.ny.dfs.hr.service.NoSuchPostingException if a matching posting could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Posting findByJ_T_D_L_H_First(
		java.lang.String jobId, java.lang.String title,
		java.lang.String description, java.lang.String lineManagers,
		java.lang.String hrManagers,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchPostingException {
		return getPersistence()
				   .findByJ_T_D_L_H_First(jobId, title, description,
			lineManagers, hrManagers, orderByComparator);
	}

	/**
	* Finds the last posting in the ordered set where jobId = &#63; and title = &#63; and description = &#63; and lineManagers = &#63; and hrManagers = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param jobId the job ID to search with
	* @param title the title to search with
	* @param description the description to search with
	* @param lineManagers the line managers to search with
	* @param hrManagers the hr managers to search with
	* @param orderByComparator the comparator to order the set by
	* @return the last matching posting
	* @throws gov.ny.dfs.hr.service.NoSuchPostingException if a matching posting could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Posting findByJ_T_D_L_H_Last(
		java.lang.String jobId, java.lang.String title,
		java.lang.String description, java.lang.String lineManagers,
		java.lang.String hrManagers,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchPostingException {
		return getPersistence()
				   .findByJ_T_D_L_H_Last(jobId, title, description,
			lineManagers, hrManagers, orderByComparator);
	}

	/**
	* Finds the postings before and after the current posting in the ordered set where jobId = &#63; and title = &#63; and description = &#63; and lineManagers = &#63; and hrManagers = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param postingId the primary key of the current posting
	* @param jobId the job ID to search with
	* @param title the title to search with
	* @param description the description to search with
	* @param lineManagers the line managers to search with
	* @param hrManagers the hr managers to search with
	* @param orderByComparator the comparator to order the set by
	* @return the previous, current, and next posting
	* @throws gov.ny.dfs.hr.service.NoSuchPostingException if a posting with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Posting[] findByJ_T_D_L_H_PrevAndNext(
		long postingId, java.lang.String jobId, java.lang.String title,
		java.lang.String description, java.lang.String lineManagers,
		java.lang.String hrManagers,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchPostingException {
		return getPersistence()
				   .findByJ_T_D_L_H_PrevAndNext(postingId, jobId, title,
			description, lineManagers, hrManagers, orderByComparator);
	}

	/**
	* Finds all the postings.
	*
	* @return the postings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Posting> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Finds a range of all the postings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of postings to return
	* @param end the upper bound of the range of postings to return (not inclusive)
	* @return the range of postings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Posting> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Finds an ordered range of all the postings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of postings to return
	* @param end the upper bound of the range of postings to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of postings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Posting> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the postings where jobId = &#63; from the database.
	*
	* @param jobId the job ID to search with
	* @throws SystemException if a system exception occurred
	*/
	public void removeByJobId(java.lang.String jobId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByJobId(jobId);
	}

	/**
	* Removes all the postings where title = &#63; from the database.
	*
	* @param title the title to search with
	* @throws SystemException if a system exception occurred
	*/
	public void removeByTitle(java.lang.String title)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByTitle(title);
	}

	/**
	* Removes all the postings where description = &#63; from the database.
	*
	* @param description the description to search with
	* @throws SystemException if a system exception occurred
	*/
	public void removeByDescription(java.lang.String description)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByDescription(description);
	}

	/**
	* Removes all the postings where jobId = &#63; and title = &#63; from the database.
	*
	* @param jobId the job ID to search with
	* @param title the title to search with
	* @throws SystemException if a system exception occurred
	*/
	public void removeByJ_T(java.lang.String jobId,
		java.lang.String title)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByJ_T(jobId, title);
	}

	/**
	* Removes all the postings where jobId = &#63; and title = &#63; and description = &#63; and lineManagers = &#63; and hrManagers = &#63; from the database.
	*
	* @param jobId the job ID to search with
	* @param title the title to search with
	* @param description the description to search with
	* @param lineManagers the line managers to search with
	* @param hrManagers the hr managers to search with
	* @throws SystemException if a system exception occurred
	*/
	public void removeByJ_T_D_L_H(java.lang.String jobId,
		java.lang.String title, java.lang.String description,
		java.lang.String lineManagers, java.lang.String hrManagers)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence()
			.removeByJ_T_D_L_H(jobId, title, description, lineManagers,
			hrManagers);
	}

	/**
	* Removes all the postings from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Counts all the postings where jobId = &#63;.
	*
	* @param jobId the job ID to search with
	* @return the number of matching postings
	* @throws SystemException if a system exception occurred
	*/
	public int countByJobId(java.lang.String jobId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByJobId(jobId);
	}

	/**
	* Counts all the postings where title = &#63;.
	*
	* @param title the title to search with
	* @return the number of matching postings
	* @throws SystemException if a system exception occurred
	*/
	public int countByTitle(java.lang.String title)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByTitle(title);
	}

	/**
	* Counts all the postings where description = &#63;.
	*
	* @param description the description to search with
	* @return the number of matching postings
	* @throws SystemException if a system exception occurred
	*/
	public int countByDescription(java.lang.String description)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByDescription(description);
	}

	/**
	* Counts all the postings where jobId = &#63; and title = &#63;.
	*
	* @param jobId the job ID to search with
	* @param title the title to search with
	* @return the number of matching postings
	* @throws SystemException if a system exception occurred
	*/
	public int countByJ_T(java.lang.String jobId, java.lang.String title)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByJ_T(jobId, title);
	}

	/**
	* Counts all the postings where jobId = &#63; and title = &#63; and description = &#63; and lineManagers = &#63; and hrManagers = &#63;.
	*
	* @param jobId the job ID to search with
	* @param title the title to search with
	* @param description the description to search with
	* @param lineManagers the line managers to search with
	* @param hrManagers the hr managers to search with
	* @return the number of matching postings
	* @throws SystemException if a system exception occurred
	*/
	public int countByJ_T_D_L_H(java.lang.String jobId,
		java.lang.String title, java.lang.String description,
		java.lang.String lineManagers, java.lang.String hrManagers)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByJ_T_D_L_H(jobId, title, description, lineManagers,
			hrManagers);
	}

	/**
	* Counts all the postings.
	*
	* @return the number of postings
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	/**
	* Gets all the candidates associated with the posting.
	*
	* @param pk the primary key of the posting to get the associated candidates for
	* @return the candidates associated with the posting
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Candidate> getCandidates(
		long pk) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getCandidates(pk);
	}

	/**
	* Gets a range of all the candidates associated with the posting.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the posting to get the associated candidates for
	* @param start the lower bound of the range of postings to return
	* @param end the upper bound of the range of postings to return (not inclusive)
	* @return the range of candidates associated with the posting
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Candidate> getCandidates(
		long pk, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getCandidates(pk, start, end);
	}

	/**
	* Gets an ordered range of all the candidates associated with the posting.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the posting to get the associated candidates for
	* @param start the lower bound of the range of postings to return
	* @param end the upper bound of the range of postings to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of candidates associated with the posting
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Candidate> getCandidates(
		long pk, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getCandidates(pk, start, end, orderByComparator);
	}

	/**
	* Gets the number of candidates associated with the posting.
	*
	* @param pk the primary key of the posting to get the number of associated candidates for
	* @return the number of candidates associated with the posting
	* @throws SystemException if a system exception occurred
	*/
	public int getCandidatesSize(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getCandidatesSize(pk);
	}

	/**
	* Determines if the candidate is associated with the posting.
	*
	* @param pk the primary key of the posting
	* @param candidatePK the primary key of the candidate
	* @return <code>true</code> if the candidate is associated with the posting; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsCandidate(long pk, long candidatePK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsCandidate(pk, candidatePK);
	}

	/**
	* Determines if the posting has any candidates associated with it.
	*
	* @param pk the primary key of the posting to check for associations with candidates
	* @return <code>true</code> if the posting has any candidates associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsCandidates(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsCandidates(pk);
	}

	private PostingPersistence getPersistence()
	{
		return getPostingPersistence();
	}

}