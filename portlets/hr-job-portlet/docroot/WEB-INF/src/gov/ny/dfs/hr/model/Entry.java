package gov.ny.dfs.hr.model;

public final class Entry {
	private long id = -1;
	private String value = "";

	/**
	 * immutable class
	 */
	private Entry(){}

	public Entry(long id, String value)
	{
		this.setId(id);
		this.setValue(value);
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = (value==null)?"":value;
	}
	
	public static Entry factory(long id, String value)
	{
		return new Entry(id, value);
	}

	@Override
	public int hashCode() {
		return (int) (id + value.hashCode());
	}
	
}
