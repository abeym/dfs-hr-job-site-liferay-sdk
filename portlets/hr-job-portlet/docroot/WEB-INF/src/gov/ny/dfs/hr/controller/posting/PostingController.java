/*
 * This file is part of hr-jobapp-portlet.
 *
 * You should have received a copy of the GNU General Public License along with
 * hr-job-portlet. If not, see <http://www.gnu.org/licenses/>.
 */
package gov.ny.dfs.hr.controller.posting;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.*;
import com.liferay.portal.kernel.json.*;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.*;
import com.liferay.portal.model.User;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;

import gov.ny.dfs.hr.controller.DFSController;
import gov.ny.dfs.hr.model.Entry;
import gov.ny.dfs.hr.service.model.*;
import gov.ny.dfs.hr.service.service.*;
import gov.ny.dfs.hr.util.HRUtil;

import java.text.*;
import java.util.*;

import javax.portlet.*;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.*;

/**
 * Handles requests for the view mode.
 */
@Controller
@RequestMapping("VIEW")
public class PostingController extends DFSController {

	private static final Logger _log = LoggerFactory.getLogger(PostingController.class);

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RenderMapping
	public String render(Locale locale, Model model, RenderRequest req, RenderResponse res) {
		_log.info("Starting render: " + locale.toString());

		String path = ParamUtil.getString(req, "path");
		if(Validator.isNotNull(path))
		{
			return path;
		}
		/*req.setAttribute("tabs1", "Postings");
		return "tabPostings";*/

		HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(req));
		String jobId = originalRequest.getParameter("jobId"); 
		if (Validator.isNotNull(jobId)) {
			List<Posting> posting;
			try {
				posting = PostingLocalServiceUtil.findByJobId(jobId);
				if((posting != null) && (posting.size()==1))
				{
					req.setAttribute("postingId", posting.get(0).getPostingId());
					return "viewPosting";
				}
			} catch (SystemException e) {
				_log.error(e.getMessage());
			}
		}

		return "listPostings";
	}
	
	@RenderMapping(params = "path=editPosting")
	public String editPosting(Locale locale, Model model, PortletRequest req, PortletResponse res) {
		_log.info("Starting editPosting: " + locale.toString());

		return "editPosting";
	}

	@RenderMapping(params = "path=listCandidates")
	public String listCandidates(Locale locale, Model model, PortletRequest req, PortletResponse res) {
		_log.info("Starting listCandidates: " + locale.toString());

		return "listCandidates";
	}
	
	private boolean isValidForm(ActionRequest req, ActionResponse res)
	{
		boolean validForm = true;
		String jobId = ParamUtil.getString(req, "jobId");
		String title = ParamUtil.getString(req, "title");
		DateFormat dateFormat = HRUtil.DATE_FORMAT;
		Date openDate = ParamUtil.getDate(req, "openDate", dateFormat);
		String lineManagers = ParamUtil.getString(req, "lineManagers");
		String hrManagers = ParamUtil.getString(req, "hrManagers");
		if(Validator.isNull(jobId))
		{
			validForm = false;
			SessionErrors.add(req, "validate.posting.jobId.required");
		}
		if(Validator.isNull(title))
		{
			validForm = false;
			SessionErrors.add(req, "validate.posting.title.required");
		}
		if(Validator.isNull(openDate))
		{
			validForm = false;
			SessionErrors.add(req, "validate.posting.openDate.required");
		}
		if(Validator.isNull(lineManagers))
		{
			validForm = false;
			SessionErrors.add(req, "validate.posting.lineManagers.required");
		}
		if(Validator.isNull(hrManagers))
		{
			validForm = false;
			SessionErrors.add(req, "validate.posting.hrManagers.required");
		}
		if(!validForm)
		{
			HRUtil.copyRequestParameters(req, res);
		}
		return validForm;
	}

	@ActionMapping(params = "action=addPosting")
	public void addPosting(Locale locale, Model model, ActionRequest req, ActionResponse res) throws SystemException {
		_log.info("Starting addPosting: " + locale.toString() + ":" + model);
		boolean validForm = true;
		String jobId = ParamUtil.getString(req, "jobId");
		String title = ParamUtil.getString(req, "title");
		DateFormat dateFormat = HRUtil.DATE_FORMAT;
		Date openDate = ParamUtil.getDate(req, "openDate", dateFormat);
		String lineManagers = ParamUtil.getString(req, "lineManagers");
		String hrManagers = ParamUtil.getString(req, "hrManagers");
		validForm = isValidForm(req, res);
		if(!validForm)
		{
			/*go back to same page.*/
			res.setRenderParameter("path", "editPosting");
			return;
		}

		Posting posting = PostingLocalServiceUtil.createPosting(CounterLocalServiceUtil.increment(Posting.class.getName()));
		posting.setJobId(jobId);
		posting.setTitle(title);
		posting.setDescription(ParamUtil.getString(req, "description"));
		posting.setOpenDate(openDate);
		posting.setCloseDate(ParamUtil.getDate(req, "closeDate", dateFormat));
		posting.setLineManagers(lineManagers);
		posting.setHrManagers(hrManagers);
		//audit fields
		ThemeDisplay themeDisplay = (ThemeDisplay) req.getAttribute(WebKeys.THEME_DISPLAY);
		User user = themeDisplay.getUser();
		posting.setUserId(user.getUserId());
		posting.setUserName(user.getScreenName());
		posting.setCompanyId(themeDisplay.getCompanyId());
		Date currentDate = new Date();
		posting.setCreateDate(currentDate);
		//save
		PostingLocalServiceUtil.addPosting(posting);
		HRUtil.createPostingFolder(req, posting.getPostingId());
		HRUtil.clearCache();
	}

	@ActionMapping(params = "action=updatePosting")
	public void updatePosting(Locale locale, Model model, ActionRequest req, ActionResponse res) throws SystemException, PortalException {
		_log.info("Starting updatePosting: " + locale.toString() + ":" + model);
		boolean validForm = isValidForm(req, res);
		if(!validForm)
		{
			/*go back to same page.*/
			res.setRenderParameter("path", "editPosting");
			return;
		}
		Posting posting = PostingLocalServiceUtil.getPosting(ParamUtil.getLong(req, "postingId"));
		DateFormat dateFormat = HRUtil.DATE_FORMAT;
		posting.setJobId(ParamUtil.getString(req, "jobId"));
		posting.setTitle(ParamUtil.getString(req, "title"));
		posting.setDescription(ParamUtil.getString(req, "description"));
		posting.setOpenDate(ParamUtil.getDate(req, "openDate", dateFormat));
		posting.setCloseDate(ParamUtil.getDate(req, "closeDate", dateFormat));
		posting.setLineManagers(ParamUtil.getString(req, "lineManagers"));
		posting.setHrManagers(ParamUtil.getString(req, "hrManagers"));
		PostingLocalServiceUtil.updatePosting(posting, true);
		((ActionResponse)res).setRenderParameter("path", "editPosting"); 
		((ActionResponse)res).setRenderParameter("postingId", ParamUtil.getString(req, "postingId")); 
		HRUtil.clearCache();
	}

	@ActionMapping(params = "action=deletePosting")
	public void deletePosting(Locale locale, Model model, PortletRequest req, PortletResponse res) throws PortalException, SystemException {
		_log.info("Starting deletePosting: " + locale.toString() + ":" + model);
		long postingId = ParamUtil.getLong(req, "postingId");
		HRUtil.deletePostingFolder(req, postingId);
		List<Candidate> deleteCandidates = CandidateLocalServiceUtil.findByPostingId(postingId);
		for(Candidate candidate : deleteCandidates)
		{
			CandidateLocalServiceUtil.deleteCandidate(candidate);
		}
		PostingLocalServiceUtil.deletePosting(postingId);
		HRUtil.clearCache();
	}


	@ActionMapping(params = "action=searchPosting")
	public void searchPosting(Locale locale, Model model, ActionRequest req, ActionResponse res) throws PortalException, SystemException {
		_log.info("Starting searchPosting: " + locale.toString() + ":" + model);
		String keywords = ParamUtil.getString(req, "keywords");
		res.setRenderParameter("keywords", keywords);
	}
	
	@ActionMapping(params = "action=searchCandidate")
	public void searchCandidate(Locale locale, Model model, ActionRequest req, ActionResponse res) throws PortalException, SystemException {
		_log.info("Starting searchCandidate: " + locale.toString() + ":" + model);
		String keywords = ParamUtil.getString(req, "keywords");
		res.setRenderParameter("keywords", keywords);
		res.setRenderParameter("path", "listCandidates"); 
		res.setRenderParameter("postingId", ParamUtil.getString(req, "postingId")); 
	}

	private void sendMail(PortletRequest req, String candidates, String subjKey, String bodyKey) throws PortalException, SystemException {
		Locale locale = req.getLocale();
		String receiverMailAddress, senderMailAddress;
		long postingId = ParamUtil.getLong(req, "postingId");
		Posting posting = PostingLocalServiceUtil.getPosting(postingId);
		receiverMailAddress = posting.getHrManagers();
		String renderURL = HtmlUtil.unescape(ParamUtil.getString(req, "renderURL"));
		ThemeDisplay td  =(ThemeDisplay)req.getAttribute(WebKeys.THEME_DISPLAY);
		User user = td.getUser();
		senderMailAddress = user.getEmailAddress();
		PortletConfig portletConfig = (PortletConfig) req.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
		String mailSubject = LanguageUtil.get(portletConfig, locale, subjKey);
		String mailBody = LanguageUtil.format(portletConfig, locale, bodyKey, new String[] { candidates, renderURL });
		HRUtil.sendMail(req, senderMailAddress, receiverMailAddress, mailSubject, mailBody);
	}

	
	@ActionMapping(params = "action=selectQualify")
	public void selectQualify(Locale locale, Model model, PortletRequest req, PortletResponse res) throws SystemException {
		_log.info("Starting selectQualify: " + locale.toString() + ":" + model);
		String[] qualifyArr = ParamUtil.getParameterValues(req, "qualifyArr");
		Map<Long, Entry> entryMap = HRUtil.toEntryMap(qualifyArr);
		Date date = new Date();
		for(Map.Entry<Long, Entry> entry : entryMap.entrySet() )
		{
			Candidate candidate;
			long candidateId = entry.getKey();
			try {
				candidate = CandidateLocalServiceUtil.getCandidate(candidateId);
				String value = entry.getValue().getValue();
				if((candidate.getQualify()==null && !"".equals(value)) || (!value.equals(candidate.getQualify())) )
				{
					candidate.setQualify(entry.getValue().getValue());
					candidate.setQualifyDate(date);
					CandidateLocalServiceUtil.updateCandidate(candidate);
				}
			} catch (PortalException e) {
				_log.error(e.getMessage());
				e.printStackTrace();
			}
		}
		((ActionResponse)res).setRenderParameter("path", "listCandidates"); 
		((ActionResponse)res).setRenderParameter("postingId", ParamUtil.getString(req, "postingId")); 
	}
		
	@ActionMapping(params = "action=selectShortlist")
	public void selectShortlist(Locale locale, Model model, PortletRequest req, PortletResponse res) throws SystemException, PortalException {
		_log.info("Starting selectShortlist: " + locale.toString() + ":" + model);
		String[] shortlistArr = ParamUtil.getParameterValues(req, "shortlistArr");
		Map<Long, Entry> entryMap = HRUtil.toEntryMap(shortlistArr);
		Date date = new Date();
		for(Map.Entry<Long, Entry> entry : entryMap.entrySet() )
		{
			Candidate candidate;
			long candidateId = entry.getKey();
			try {
				candidate = CandidateLocalServiceUtil.getCandidate(candidateId);
				String value = entry.getValue().getValue();
				if((candidate.getShortlist()==null && !"".equals(value)) || (!value.equals(candidate.getShortlist())) )
				{
					candidate.setShortlist(value);
					candidate.setShortlistDate(date);
					CandidateLocalServiceUtil.updateCandidate(candidate);
				}
			} catch (PortalException e) {
				_log.error(e.getMessage());
				e.printStackTrace();
			}
		}
		((ActionResponse)res).setRenderParameter("path", "listCandidates"); 
		((ActionResponse)res).setRenderParameter("postingId", ParamUtil.getString(req, "postingId")); 
	}

	@ActionMapping(params = "action=selectInterview")
	public void selectInterview(Locale locale, Model model, PortletRequest req, PortletResponse res) throws SystemException {
		_log.info("Starting selectInterview: " + locale.toString() + ":" + model);
		String[] interviewArr = ParamUtil.getParameterValues(req, "interviewArr");
		String interviewDateJson = ParamUtil.getString(req, "interviewDateJson");
		Map<Long, Entry>intvwDates = new HashMap<Long, Entry>();
		try {
			JSONObject jsob = JSONFactoryUtil.createJSONObject(interviewDateJson);
			intvwDates = HRUtil.toMap(jsob, res.getNamespace()+"interviewDate");
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Map<Long, Entry> entryMap = HRUtil.toEntryMap(interviewArr);
		Date date = new Date();
		for(Map.Entry<Long, Entry> entry : entryMap.entrySet() )
		{
			Candidate candidate;
			long candidateId = entry.getKey();
			try {
				candidate = CandidateLocalServiceUtil.getCandidate(candidateId);
				String value = entry.getValue().getValue();
				boolean modified = false;
				if((candidate.getInterview()==null && !"".equals(value)) || (!value.equals(candidate.getInterview())) )
				{
					candidate.setInterview(entry.getValue().getValue());
					candidate.setInterviewDate(date);
					modified = true;
				}
				try {
					Date interviewDate = HRUtil.DATE_TIME_FORMAT.parse(intvwDates.get(candidateId).getValue());
					if(!interviewDate.equals(candidate.getInterviewDate()))
					{
						candidate.setInterviewDate(interviewDate);
						modified = true;
					}
				} catch (Exception e) {
					_log.error(e.getMessage());
				}
				if(modified)
				{
					CandidateLocalServiceUtil.updateCandidate(candidate);
				}
			} catch (PortalException e) {
				_log.error(e.getMessage());
				e.printStackTrace();
			}
		}
		((ActionResponse)res).setRenderParameter("path", "listCandidates"); 
		((ActionResponse)res).setRenderParameter("postingId", ParamUtil.getString(req, "postingId")); 
	}

	@ActionMapping(params = "action=deleteCandidate")
	public void deleteCandidate(Locale locale, Model model, ActionRequest req, ActionResponse res) throws Exception {
		_log.info("Starting deleteCandidate: " + locale.toString());
		Candidate candidate = CandidateLocalServiceUtil.getCandidate(ParamUtil.getLong(req, "candidateId"));
		CandidateLocalServiceUtil.deleteCandidate(candidate);
		((ActionResponse)res).setRenderParameter("path", "listCandidates"); 
		((ActionResponse)res).setRenderParameter("postingId", String.valueOf(candidate.getPostingId())); 
	}
}
