/*
 * This file is part of hr-joapp-portlet.
 *
 * You should have received a copy of the GNU General Public License along with
 * hr-job-portlet. If not, see <http://www.gnu.org/licenses/>.
 */
package gov.ny.dfs.hr.controller.application;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.documentlibrary.FileSizeException;
import com.liferay.portal.kernel.captcha.CaptchaException;
import com.liferay.portal.kernel.exception.*;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.*;
import com.liferay.portal.model.User;
import com.liferay.portal.service.*;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.model.*;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.liferay.util.portlet.PortletProps;

import gov.ny.dfs.hr.controller.DFSController;
import gov.ny.dfs.hr.model.application.CandidateForm;
import gov.ny.dfs.hr.service.model.*;
import gov.ny.dfs.hr.service.service.*;
import gov.ny.dfs.hr.util.HRUtil;

import java.io.*;
import java.io.File;
import java.util.*;

import javax.portlet.*;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.*;

/**
 * Handles requests for the view mode.
 */
@Controller
@RequestMapping("VIEW")
public class ApplicationController extends DFSController {

	private static final Logger _log = LoggerFactory.getLogger(ApplicationController.class);

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RenderMapping
	public String render(Locale locale, Model model, PortletRequest req, PortletResponse res) {
		_log.info("Starting Controller: " + locale.toString());

		String path = ParamUtil.getString(req, "path");
		if (Validator.isNotNull(path)) {
			return path;
		}
		
		//
//		String jobId = ParamUtil.getString(req, "jobId");
		HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(req));
		String jobId = originalRequest.getParameter("jobId"); 
		if (Validator.isNotNull(jobId)) {
			List<Posting> posting;
			try {
				posting = PostingLocalServiceUtil.findByJobId(jobId);
				if((posting != null) && (posting.size()==1))
				{
					req.setAttribute("postingId", posting.get(0).getPostingId());
					return "viewApplication";
				}
			} catch (SystemException e) {
				_log.error(e.getMessage());
			}
		}
		

		return "listApplications";
	}

	@RenderMapping(params = "path=addApplication")
	public String addApplication(Locale locale, Model model, RenderRequest req, RenderResponse res) {
		_log.info("Starting Controller: " + locale.toString());
		//req.setAttribute("candidate", new CandidateImpl());
		/*ThemeDisplay td = (ThemeDisplay) req.getAttribute(WebKeys.THEME_DISPLAY);
		Company company = td.getCompany();
		User doAsUser = HRUtil.getDoAsUser(req);
		//String doAsUserId = Encryptor.encrypt( company.getKeyObj(), String.valueOf(doAsUser.getUserId()));
		req.setAttribute("doAsUserId", doAsUser.getUserId());*/
		return "addApplication";
	}

	@RenderMapping(params = "path=editCandidate")
	public String editCandidate(Locale locale, Model model, PortletRequest req, PortletResponse res) {
		_log.info("Starting Controller: " + locale.toString());

		return "editCandidate";
	}

	@ActionMapping(params = "action=confirmCandidate")
	public void confirmCandidate(/*@Valid*/ CandidateForm model, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception{
		_log.info("Starting confirmCandidate: " );
		//do nothing. portlet renders list of open positions by default.
		actionResponse.setRenderParameter("path", "listApplications"); 
	}
	
	@ActionMapping(params = "action=addCandidate")
	public void addCandidate(/*@Valid*/ CandidateForm model, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception{
		_log.info("Starting addCandidate: " );
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		
		//validations
		boolean validForm = true;
		String name = ParamUtil.getString(uploadRequest, "name");
		String email = ParamUtil.getString(uploadRequest, "email");
		String phoneNum = ParamUtil.getString(uploadRequest, "phoneNum");
		String confirmationNum = HRUtil.nextConfirmationNum();
		String resumeText = ParamUtil.getString(uploadRequest, "resumeText");
		Set<DLFileEntry> resumeFiles = addMultiFile(uploadRequest, actionRequest, actionResponse, confirmationNum, "resumeFile");
		String coverLetterText = ParamUtil.getString(uploadRequest, "coverLetter");
		Set<DLFileEntry> coverLetterFiles = addMultiFile(uploadRequest, actionRequest, actionResponse, confirmationNum, "coverLetterFile");
		long postingId = ParamUtil.getLong(uploadRequest, "postingId");
		try
		{
			/*
			//check recaptcha is valid.
			com.liferay.portal.kernel.captcha.CaptchaUtil.check(actionRequest);
			*/
			checkCaptcha(uploadRequest, actionRequest);
			if(!Validator.isName(name))
			{
				validForm = false;
				SessionErrors.add(actionRequest, "validate.application.name.fullName");
			}
			if(!Validator.isEmailAddress(email))
			{
				validForm = false;
				SessionErrors.add(actionRequest, "validate.application.email.valid");
			}
			if(!Validator.isPhoneNumber(phoneNum))
			{
				validForm = false;
				SessionErrors.add(actionRequest, "validate.application.phone.valid");
			}
			if(Validator.isNull(resumeText) && (resumeFiles.isEmpty()))
			{
				validForm = false;
				SessionErrors.add(actionRequest, "validate.application.resume.text.required");
			}
		}
		catch(Exception e)
		{
			validForm = false;
			SessionErrors.add(actionRequest, e.getClass().getName(), e);
		}
		if(!validForm)
		{
			/*go back to same page.*/
			actionResponse.setRenderParameter("postingId", String.valueOf(postingId));
			actionResponse.setRenderParameter("path", "addApplication");
			HRUtil.copyRequestParameters(uploadRequest, actionResponse);
			return;
		}

		Posting posting = PostingLocalServiceUtil.getPosting(postingId);
		Candidate candidate = CandidateLocalServiceUtil.createCandidate(CounterLocalServiceUtil.increment(Candidate.class.getName()));
		candidate.setName(name);
		candidate.setEmail(email);
		candidate.setPhoneNum(phoneNum);
		candidate.setCoverLetter(coverLetterText);
		candidate.setCoverLetterCSV(getFilesCSV(coverLetterFiles));
		candidate.setResume(getFilesCSV(resumeFiles));
		candidate.setResumeText(resumeText);
		candidate.setPostingId(postingId);
		candidate.setSubmitDate(new Date());
		candidate.setConfirmationNum(confirmationNum);
		//audit fields
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		User user = themeDisplay.getUser();
		candidate.setUserId(user.getUserId());
		candidate.setUserName(user.getScreenName());
		candidate.setCompanyId(themeDisplay.getCompanyId());
		Date currentDate = new Date();
		candidate.setCreateDate(currentDate);
		//save
		CandidateLocalServiceUtil.addCandidate(candidate);
		sendCandidateMail(actionRequest, posting.getTitle(), candidate.getName(), candidate.getEmail());
		actionResponse.setRenderParameter("path", "confirmApplication"); 
	}

	private String getFilesCSV(Set<DLFileEntry> multiFiles) {
		StringBuffer filesCSV = new StringBuffer();
		int i = 0;
		for (DLFileEntry file : multiFiles) {
			if (i++ > 0)
				filesCSV.append(",");
			filesCSV.append(file.getUuid());
		}
		return filesCSV.toString();
	}

	private Set<DLFileEntry> addMultiFile(UploadPortletRequest uploadRequest, ActionRequest actionRequest, ActionResponse actionResponse, String confirmNum, String inputName) {
		_log.info("Starting addMultiFile: ");
		Map<String, File> fileMap = new LinkedHashMap<String, File>();
		File tempFile;
		Enumeration<String> paramEnum = uploadRequest.getParameterNames();
		int i=1;
		while (paramEnum.hasMoreElements()) {
			String parameter = paramEnum.nextElement();
			if (parameter.startsWith(inputName)) {
				tempFile = uploadRequest.getFile(parameter);
				String ext = FilenameUtils.getExtension(tempFile.getName());
				tempFile = renameFileName(tempFile, uploadRequest, confirmNum+"_"+i++ + "." + ext);
				if (tempFile != null) {
					fileMap.put(parameter, tempFile);
				}
			}
		}
		Set<DLFileEntry> multiFiles = new HashSet<DLFileEntry>();
		for (Map.Entry<String, File> entry : fileMap.entrySet()) {
			try {
				multiFiles.add(singleFileUpload(uploadRequest, actionRequest, entry.getValue()));
			} catch (PortalException e) {
				_log.error(e.getMessage());
				e.printStackTrace();
			} catch (SystemException e) {
				_log.error(e.getMessage());
				e.printStackTrace();
			}
		}
		return multiFiles;
	}

	private File renameFileName(File sourceFile, UploadPortletRequest uploadRequest, String fileNm) {
		File destination = null;
		String path = sourceFile.getPath();
		path = path.substring(0, path.lastIndexOf(StringPool.BACK_SLASH) + 1);
		if ( (sourceFile.length()>0) && Validator.isNotNull(fileNm)) {
			path = path.concat(fileNm);
			destination = new File(path);
			FileUtil.copyFile(sourceFile, destination);
			FileUtil.delete(sourceFile);
		}
		return destination;
	}

	private DLFileEntry singleFileUpload(UploadPortletRequest uploadRequest, ActionRequest actionRequest, File file) throws PortalException,
			SystemException {
		ThemeDisplay themeDisplay = (ThemeDisplay) uploadRequest.getAttribute(WebKeys.THEME_DISPLAY);
		DLFileEntry fileEntry = null;
		String title = file.getName();
		long postingId = Long.valueOf((String) ParamUtil.getString(uploadRequest, "postingId"));
		DLFolder postingFolder = HRUtil.getPostingFolder(actionRequest, postingId);
		ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFileShortcut.class.getName(), actionRequest);
		long groupId = themeDisplay.getScopeGroupId();
		try {
			long doAsUserId = HRUtil.getDoAsUser(actionRequest).getUserId();
			if (postingFolder == null) {
				postingFolder = HRUtil.createPostingFolder(actionRequest, postingId);
			}
			if (postingFolder != null) {
				fileEntry = DLFileEntryLocalServiceUtil.addFileEntry(doAsUserId, groupId, postingFolder.getFolderId(), file.getName(), title, StringPool.BLANK, null, null,
						file, serviceContext);
			}
		} catch(FileSizeException e)
		{
			_log.error(e.getMessage());
			e.printStackTrace();
			throw e;
		}
		catch (Exception e) {
			_log.error(e.getMessage());
			e.printStackTrace();
		}
		return fileEntry;
	}

	@ResourceMapping("captchaUrl")
	public void captchaUrl(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		try {
			com.liferay.portal.kernel.captcha.CaptchaUtil.serveImage(resourceRequest, resourceResponse);
		} catch (Exception e) {
			_log.error(e.getMessage());
		}
	}

	private void checkCaptcha(UploadPortletRequest uploadRequest, ActionRequest actionRequest) throws CaptchaException {
		String enteredCaptchaText = ParamUtil.getString(uploadRequest, "captchaText");
		//String enteredCaptchaText = ParamUtil.getString(uploadRequest, "recaptcha_response_field");

		PortletSession session = actionRequest.getPortletSession();
		String captchaText = getCaptchaValueFromSession(session);
		if (Validator.isNull(captchaText)) {
			throw new CaptchaException("Internal Error! Captcha text not found in session");
		}
		if (!HRUtil.equals(captchaText, enteredCaptchaText)) {
			Locale locale = actionRequest.getLocale();
			PortletConfig portletConfig = (PortletConfig) actionRequest.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
			String msg = LanguageUtil.get(portletConfig, locale, "validate.application.captcha.valid");
			throw new CaptchaException(msg);
		}
	}

	private String getCaptchaValueFromSession(PortletSession session) {
		Enumeration<String> atNames = session.getAttributeNames();
		while (atNames.hasMoreElements()) {
			String name = atNames.nextElement();
			if (name.contains("CAPTCHA_TEXT")) {
				return (String) session.getAttribute(name);
			}
		}
		return null;
	}

	private void sendCandidateMail(ActionRequest req, String positingTitle, String name, String receiverMailAddress) {
		Locale locale = req.getLocale();
		PortletConfig portletConfig = (PortletConfig) req.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
		String mailSubject = LanguageUtil.get(portletConfig, locale, "mail-candidate-Subject");
		String mailBody = LanguageUtil.format(portletConfig, locale, "mail-candidate-Body", new String[] { name, positingTitle });
		String senderMailAddress = PortletProps.get("gov.ny.dfs.hr.mail.senderAddess");
		HRUtil.sendMail(req, senderMailAddress, receiverMailAddress, mailSubject, mailBody);
	}

	@ActionMapping(params = "action=searchPosting")
	public void searchPosting(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception{
		_log.info("Starting searchPosting: " );
		String keywords = ParamUtil.getString(actionRequest, "keywords");
		actionResponse.setRenderParameter("keywords", keywords);
	}

}
