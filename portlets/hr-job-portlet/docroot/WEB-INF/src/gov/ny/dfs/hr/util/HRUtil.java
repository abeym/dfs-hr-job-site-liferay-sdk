package gov.ny.dfs.hr.util;

import com.liferay.mail.service.MailServiceUtil;
import com.liferay.portal.kernel.cache.MultiVMPoolUtil;
import com.liferay.portal.kernel.dao.orm.*;
import com.liferay.portal.kernel.exception.*;
import com.liferay.portal.kernel.json.*;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.mail.MailMessage;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.*;
import com.liferay.portal.model.*;
import com.liferay.portal.service.*;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.model.*;
import com.liferay.portlet.documentlibrary.service.DLFolderLocalServiceUtil;
import com.liferay.portlet.expando.model.ExpandoTableConstants;
import com.liferay.portlet.imagegallery.model.IGImage;
import com.liferay.util.portlet.PortletProps;

import gov.ny.dfs.hr.model.Entry;
import gov.ny.dfs.hr.service.model.Posting;
import gov.ny.dfs.hr.service.service.PostingLocalServiceUtil;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.*;
import java.util.concurrent.locks.Lock;

import javax.mail.internet.InternetAddress;
import javax.portlet.*;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.*;

public class HRUtil {
	private static final Logger _log = LoggerFactory.getLogger(HRUtil.class);
	public static final String TRUE = "Y";
	public static final String FALSE = "N";
	private static SecureRandom random = new SecureRandom();
	public static final DateFormat DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy");
	public static final DateFormat DATE_TIME_FORMAT = new SimpleDateFormat("MM/dd/yyyy HH:mm");
	private static final Lock lock = new ReentrantLock();

	/**
	 * @param a
	 * @param b
	 * @return
	 */
	public static boolean equals(String a, String b) {
		if ((a == null) || (b == null)) {
			return false;
		}
		return a.equals(b);
	}

	/**
	 * @param df
	 * @param date
	 * @return
	 */
	public static String format(Date date) {
		return format(date, DATE_FORMAT);
	}

	/**
	 * @param df
	 * @param date
	 * @return
	 */
	public static String format(Date date, DateFormat df) {
		String retVal = "";
		try {
			retVal = df.format(date);
		} catch (Exception e) {
			_log.trace(e.getMessage());
		}
		return retVal;
	}


	
	/**
	 * @param portletRequest
	 * @param paramName
	 * @return
	 */
	public static Object getParamsValues(PortletRequest portletRequest, String paramName) {
		Object obj = null;
		obj = ParamUtil.getString(portletRequest, paramName);
		if (obj != null)
			return obj;
		obj = portletRequest.getAttribute(paramName);
		if (obj != null)
			return obj;
		obj = portletRequest.getParameter(paramName);
		if (obj != null)
			return obj;

		HttpServletRequest servletRequest = PortalUtil.getHttpServletRequest(portletRequest);
		obj = ParamUtil.getString(servletRequest, paramName);
		if (obj != null)
			return obj;
		obj = servletRequest.getAttribute(paramName);
		if (obj != null)
			return obj;
		obj = servletRequest.getParameter(paramName);
		if (obj != null)
			return obj;

		return obj;
	}

	/**
	 * @return
	 */
	public static String nextConfirmationNum() {
		return new BigInteger(130, random).toString(32);
	}

	/**
	 * @param portletRequest
	 * @param postingId
	 */
	public static void deletePostingFolder(PortletRequest portletRequest, long postingId) {
		DLFolder postingFolder = getPostingFolder(portletRequest, postingId);
		try {
			if(postingFolder != null)
			{
				DLFolderLocalServiceUtil.deleteFolder(postingFolder.getFolderId());
			}
		} catch (Exception e) {
			_log.error(e.getMessage());
		}
	}
	
	/**
	 * @param portletRequest
	 * @param postingId
	 * @return
	 */
	public static DLFolder getPostingFolder(PortletRequest portletRequest, long postingId) {
		DLFolder dlFolder = null;
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
			long groupId = themeDisplay.getScopeGroupId();
			Posting posting = PostingLocalServiceUtil.getPosting(postingId);
			String postingTitle = posting.getTitle();
			String mainFolder = PortletProps.get("gov.ny.dfs.hr.dl.folder");
			dlFolder = DLFolderLocalServiceUtil.getFolder(groupId, 0, mainFolder);
			long parentFolderId = dlFolder.getFolderId();
			dlFolder = DLFolderLocalServiceUtil.getFolder(groupId, parentFolderId, postingTitle);
		} catch (Exception e) {
			_log.error(e.getMessage());
		}
		return dlFolder;
	}

	/**
	 * return property
	 * @param property
	 * @return 
	*/
	public static String getProps(String property)
	{
		String val = PropsUtil.get(property);
		if(val == null)
		{
			val = PortletProps.get(property);
		}
		return val;
	}
	
	/**
	 * @param portletRequest
	 * @param postingId
	 * @return
	 */
	public static DLFolder createPostingFolder(PortletRequest portletRequest, long postingId) {
		DLFolder dlFolder = null;
		String mainFolder = PortletProps.get("gov.ny.dfs.hr.dl.folder");
		String postingTitle = PortletProps.get("gov.ny.dfs.hr.dl.folder.misc");
		try {
			Posting posting = PostingLocalServiceUtil.getPosting(postingId);
			postingTitle = posting.getTitle();
			ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFileShortcut.class.getName(), portletRequest);
			ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
			long groupId = themeDisplay.getScopeGroupId();
			long parentFolderId = 0;
			try {
				dlFolder = DLFolderLocalServiceUtil.getFolder(groupId, 0, mainFolder);
				parentFolderId = dlFolder.getFolderId();
			} catch (Exception e) {
				try {
					User doAsUser = getDoAsUser(portletRequest);
					dlFolder = DLFolderLocalServiceUtil.addFolder(doAsUser.getUserId(), groupId, parentFolderId, mainFolder, "", serviceContext);
					parentFolderId = dlFolder.getFolderId();
				} catch (SystemException e1) {
					_log.error(e1.getMessage());
				} catch (PortalException e1) {
					_log.error(e1.getMessage());
				}
			}
			// Posting folder
			try {
				dlFolder = DLFolderLocalServiceUtil.getFolder(groupId, parentFolderId, postingTitle);
			} catch (Exception e) {
				try {
					User doAsUser = getDoAsUser(portletRequest);
					dlFolder = DLFolderLocalServiceUtil.addFolder(doAsUser.getUserId(), groupId, parentFolderId, postingTitle, "", serviceContext);
				} catch (SystemException e1) {
					_log.error(e1.getMessage());
				} catch (PortalException e1) {
					_log.error(e1.getMessage());
				}
			}
		} catch (Exception e) {
			_log.error(e.getMessage());
		}
		return dlFolder;
	}

	/**
	 * @param req
	 * @param senderMailAddress
	 * @param receiverMailAddress
	 * @param mailSubject
	 * @param mailBody
	 */
	public static void sendMail(PortletRequest req, String senderMailAddress, String receiverMailAddress, String mailSubject, String mailBody) {
		try {
			MailMessage mailMessage = new MailMessage();
			mailMessage.setBody(mailBody);
			mailMessage.setSubject(mailSubject);
			mailMessage.setFrom(new InternetAddress(senderMailAddress));
			String[] receivers = StringUtil.split(receiverMailAddress, ",");
			int i = 0;
			for(String receiver : receivers)
			{
				if(Validator.isNotNull(receiver))
				{
					if(i++ > 0)
					{
						mailMessage.setCC(new InternetAddress(receiver));
					}
					else
					{
						mailMessage.setTo(new InternetAddress(receiver));
					}
				}
			}
			MailServiceUtil.sendEmail(mailMessage);
			SessionMessages.add(req.getPortletSession(), "mail-send-success");
		} catch (Exception e) {
			_log.error(e.getMessage());
		}
	}

	/**
	 * @param portletRequest
	 * @return
	 * @throws SystemException
	 * @throws PortalException
	 */
	public static String getJsonHRManagers(PortletRequest portletRequest) throws SystemException, PortalException
	{
		String userGroupName = PortletProps.get("gov.ny.dfs.hr.usergroup.hr");
		ThemeDisplay td = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		long userGroupId = UserGroupLocalServiceUtil.getUserGroup(td.getCompanyId(), userGroupName).getUserGroupId();
		List<User> userGroupUsers = UserLocalServiceUtil.getUserGroupUsers(userGroupId);
		Set<String> hrManagers = new TreeSet<String>();
		for(User user : userGroupUsers)
		{
			hrManagers.add(user.getEmailAddress());
		}
		return toJson(hrManagers.toArray(new String[]{}));
	}

	/**
	 * @param portletRequest
	 * @return
	 * @throws SystemException
	 * @throws PortalException
	 */
	public static String getJsonLineManagers(PortletRequest portletRequest) throws SystemException, PortalException
	{
		String userGroupName = PortletProps.get("gov.ny.dfs.hr.usergroup.line");
		ThemeDisplay td = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		long userGroupId = UserGroupLocalServiceUtil.getUserGroup(td.getCompanyId(), userGroupName).getUserGroupId();
		List<User> userGroupUsers = UserLocalServiceUtil.getUserGroupUsers(userGroupId);
		Set<String> hrManagers = new TreeSet<String>();
		for(User user : userGroupUsers)
		{
			hrManagers.add(user.getEmailAddress());
		}
		return toJson(hrManagers.toArray(new String[]{}));
	}

	/**
	 * @param values
	 * @return
	 */
	public static String toJson(String[] noDupsArr)
	{
		for(int i=0; i<noDupsArr.length; i++)
		{
			//A visible character: [\p{Alnum}\p{Punct}]
			noDupsArr[i] = noDupsArr[i].replaceAll("[^\\p{Graph} ]", "");
		}
		String json = JSONFactoryUtil.serialize(noDupsArr);
		return json;
	}

	/**
	 * @param req
	 * @return
	 * @throws PortalException
	 * @throws SystemException
	 */
	public static User getDoAsUser(PortletRequest req) throws PortalException, SystemException
	{
		ThemeDisplay td = (ThemeDisplay) req.getAttribute(WebKeys.THEME_DISPLAY);
		Locale locale = td.getLocale();
		Company company = td.getCompany();
		long companyId = company.getCompanyId();
		PortletConfig portletConfig = (PortletConfig) req.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
		String emailAddress = PortletProps.get("gov.ny.dfs.hr.mail.senderAddess");
		User doAsUser = UserLocalServiceUtil.getUserByEmailAddress(companyId , emailAddress);
		return doAsUser;
	}
	
	public static boolean isUserInUserGroup(User user, String userGroupName)
	{
		boolean isUserInUserGroup = false;
		try {
			long checkUserGroupId;
			checkUserGroupId = UserGroupLocalServiceUtil.getUserGroup(PortalUtil.getDefaultCompanyId(), userGroupName).getUserGroupId();
			long[] userGroupIds = user.getUserGroupIds();
			for(long userGroupId: userGroupIds)
			{
				if(userGroupId == checkUserGroupId)
				{
					return true;
				}
			}
		} catch (Exception e) {
			_log.error(e.getMessage());
		}
		return isUserInUserGroup;
	}


	public static void clearCache()
	{
		MultiVMPoolUtil.clear(HRUtil.class.getName());
	}
	
	public static String getJsonJobKeywords()
	{
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		String currDateStr = df.format(date);
		String cacheKey = "HR-JobTitles-"+currDateStr;
		String titleArr = (String) MultiVMPoolUtil.get(HRUtil.class.getName(), cacheKey);
		if(titleArr == null){
			try
			{
				lock.tryLock(10, TimeUnit.MILLISECONDS);
				titleArr = (String) MultiVMPoolUtil.get(HRUtil.class.getName(), cacheKey);
				if(titleArr == null){
					DynamicQuery dq = DynamicQueryFactoryUtil.forClass(Posting.class);
					dq.add(RestrictionsFactoryUtil.or(RestrictionsFactoryUtil.ge("closeDate", date), 
							RestrictionsFactoryUtil.isNull("closeDate")) );
					dq.add(RestrictionsFactoryUtil.or(RestrictionsFactoryUtil.le("openDate", date), 
							RestrictionsFactoryUtil.isNull("openDate")) );
					List<Posting> postings = PostingLocalServiceUtil.findWithDynamicQuery(dq);
					List<String> values = new ArrayList<String>();
					for(Posting posting: postings)
					{
						values.add(posting.getJobId());
						values.add(posting.getTitle());
					}
					String[] valuesArr = (String[]) values.toArray(new String[values.size()]);
					titleArr = toJson(valuesArr);
					 _log.debug(">>>>>>>"+cacheKey+":"+titleArr);
					MultiVMPoolUtil.put(HRUtil.class.getName(), cacheKey, titleArr);
				}
			}
			catch(InterruptedException e)
			{
				_log.error(e.getMessage(),e);
			}
			catch(SystemException e)
			{
				_log.error(e.getMessage(),e);
			}
			finally
			{
				lock.unlock();
			}
		}
		 return titleArr;
	}
	

	public static <E> Iterable<E> iterable(final Iterator<E> iterator) {
		if (iterator == null) {
			throw new NullPointerException();
		}
		return new Iterable<E>() {
			public Iterator<E> iterator() {
				return iterator;
			}
		};
	}
	  
	/**
	 * convert {"", "Y1234", "N5678"} 
	 * to map
	 * {[0,{0,""}], [1234,{1234,"Y"}],[5678,{5678,"N"}]}
	 */
	public static Map<Long, Entry> toEntryMap(String[] strArr)
	{
		Map<Long, Entry> entryMap = new HashMap<Long, Entry>();
		for(String str:strArr)
		{
			if(str!=null)
			{
				if(str.startsWith(TRUE)||str.startsWith(FALSE))
				{
					Long key = Long.valueOf(str.substring(1));
					Entry value = Entry.factory(key, str.substring(0, 1)) ;
					entryMap.put(key, value);
				}
				else
				{
					Long key = Long.valueOf(str);
					Entry value = Entry.factory(key, "") ;
					entryMap.put(key, value);
				}
			}
		}
		return entryMap;
		
	}

	public static String getAcceptValues(String shortValue, PortletRequest portletRequest)
	{
		ThemeDisplay td = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		Locale locale = td.getLocale();
		PortletConfig portletConfig = (PortletConfig) portletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
		if((shortValue == null) || ("".equals(shortValue.trim())))
		{
			return LanguageUtil.get(portletConfig, locale, "candidate-noaction");
		}
		else if("Y".equals(shortValue))
		{
			return LanguageUtil.get(portletConfig, locale, "candidate-accept");
		}
		else 
		{
			return LanguageUtil.get(portletConfig, locale, "candidate-reject");
		}
			
	}

	public static Map<Long, Entry> toMap(JSONObject jsob, String paramName) {
		Map<Long, Entry> entryMap = new HashMap<Long, Entry>();
		Iterator<String> keys = jsob.keys();
		for(String key: iterable(keys))
		{
			try
			{
				Long id = Long.valueOf(key.replace(paramName, ""));
				entryMap.put(id, new Entry(id, jsob.getString(key)));
			}
			catch(Exception e)
			{
				_log.error(e.getMessage());
			}
		}
		return entryMap;
	}

	public static void copyRequestParameters(HttpServletRequest servletRequest, ActionResponse actionResponse) {
		List<String> parameterNames = Collections.list(servletRequest.getParameterNames());
		for(String parameterName : parameterNames)
		{
			actionResponse.setRenderParameter(parameterName, ParamUtil.get(servletRequest, parameterName, ""));
		}
		
	}

	public static void copyRequestParameters(ActionRequest req, ActionResponse res) {
		//copyRequestParameters(PortalUtil.getHttpServletRequest(req), res);
		List<String> parameterNames = Collections.list(req.getParameterNames());
		for(String parameterName : parameterNames)
		{
			res.setRenderParameter(parameterName, ParamUtil.get(req, parameterName, ""));
		}
	}
}
