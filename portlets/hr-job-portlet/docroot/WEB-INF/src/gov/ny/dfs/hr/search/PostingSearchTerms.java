/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package gov.ny.dfs.hr.search;

import com.liferay.portal.kernel.dao.search.DAOParamUtil;

import javax.portlet.PortletRequest;

/**
 * @author Brian Wing Shun Chan
 */
public class PostingSearchTerms extends PostingDisplayTerms {

	public PostingSearchTerms(PortletRequest portletRequest) {
		super(portletRequest);

		description = DAOParamUtil.getLike(portletRequest, DESCRIPTION);
		title = DAOParamUtil.getLike(portletRequest, TITLE);
		jobId = DAOParamUtil.getLike(portletRequest, JOB_ID);
	}

}