/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package gov.ny.dfs.hr.service.service.impl;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.service.ServiceContext;

import gov.ny.dfs.hr.service.model.Candidate;
import gov.ny.dfs.hr.service.service.CandidateLocalService;
import gov.ny.dfs.hr.service.service.base.CandidateLocalServiceBaseImpl;
import gov.ny.dfs.hr.service.service.persistence.CandidatePersistence;

import java.util.List;

/**
 * The implementation of the candidate local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link gov.ny.dfs.hr.service.service.CandidateLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Abey
 * @see gov.ny.dfs.hr.service.service.base.CandidateLocalServiceBaseImpl
 * @see gov.ny.dfs.hr.service.service.CandidateLocalServiceUtil
 */
public class CandidateLocalServiceImpl extends CandidateLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link gov.ny.dfs.hr.service.service.CandidateLocalServiceUtil} to access the candidate local service.
	 */
	
	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public void clearCache(Candidate candidate) {
		getPersistence().clearCache(candidate);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public List<Candidate> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public List<Candidate> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public List<Candidate> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#remove(com.liferay.portal.model.BaseModel)
	 */
	public Candidate remove(Candidate candidate)
		throws SystemException {
		return getPersistence().remove(candidate);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public Candidate update(Candidate candidate, boolean merge)
		throws SystemException {
		return getPersistence().update(candidate, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public Candidate update(Candidate candidate, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(candidate, merge, serviceContext);
	}

	/**
	* Caches the candidate in the entity cache if it is enabled.
	*
	* @param candidate the candidate to cache
	*/
	public void cacheResult(
		gov.ny.dfs.hr.service.model.Candidate candidate) {
		getPersistence().cacheResult(candidate);
	}

	/**
	* Caches the candidates in the entity cache if it is enabled.
	*
	* @param candidates the candidates to cache
	*/
	public void cacheResult(
		java.util.List<gov.ny.dfs.hr.service.model.Candidate> candidates) {
		getPersistence().cacheResult(candidates);
	}

	/**
	* Creates a new candidate with the primary key. Does not add the candidate to the database.
	*
	* @param candidateId the primary key for the new candidate
	* @return the new candidate
	*/
	public gov.ny.dfs.hr.service.model.Candidate create(long candidateId) {
		return getPersistence().create(candidateId);
	}

	/**
	* Removes the candidate with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param candidateId the primary key of the candidate to remove
	* @return the candidate that was removed
	* @throws gov.ny.dfs.hr.service.NoSuchCandidateException if a candidate with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Candidate remove(long candidateId)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchCandidateException {
		return getPersistence().remove(candidateId);
	}

	public gov.ny.dfs.hr.service.model.Candidate updateImpl(
		gov.ny.dfs.hr.service.model.Candidate candidate, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(candidate, merge);
	}

	/**
	* Finds the candidate with the primary key or throws a {@link gov.ny.dfs.hr.service.NoSuchCandidateException} if it could not be found.
	*
	* @param candidateId the primary key of the candidate to find
	* @return the candidate
	* @throws gov.ny.dfs.hr.service.NoSuchCandidateException if a candidate with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Candidate findByPrimaryKey(
		long candidateId)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchCandidateException {
		return getPersistence().findByPrimaryKey(candidateId);
	}

	/**
	* Finds the candidate with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param candidateId the primary key of the candidate to find
	* @return the candidate, or <code>null</code> if a candidate with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Candidate fetchByPrimaryKey(
		long candidateId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(candidateId);
	}

	/**
	* Finds all the candidates where postingId = &#63;.
	*
	* @param postingId the posting ID to search with
	* @return the matching candidates
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Candidate> findByPostingId(
		long postingId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPostingId(postingId);
	}

	/**
	* Finds a range of all the candidates where postingId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param postingId the posting ID to search with
	* @param start the lower bound of the range of candidates to return
	* @param end the upper bound of the range of candidates to return (not inclusive)
	* @return the range of matching candidates
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Candidate> findByPostingId(
		long postingId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPostingId(postingId, start, end);
	}

	/**
	* Finds an ordered range of all the candidates where postingId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param postingId the posting ID to search with
	* @param start the lower bound of the range of candidates to return
	* @param end the upper bound of the range of candidates to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching candidates
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Candidate> findByPostingId(
		long postingId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByPostingId(postingId, start, end, orderByComparator);
	}

	/**
	* Finds the first candidate in the ordered set where postingId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param postingId the posting ID to search with
	* @param orderByComparator the comparator to order the set by
	* @return the first matching candidate
	* @throws gov.ny.dfs.hr.service.NoSuchCandidateException if a matching candidate could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Candidate findByPostingId_First(
		long postingId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchCandidateException {
		return getPersistence()
				   .findByPostingId_First(postingId, orderByComparator);
	}

	/**
	* Finds the last candidate in the ordered set where postingId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param postingId the posting ID to search with
	* @param orderByComparator the comparator to order the set by
	* @return the last matching candidate
	* @throws gov.ny.dfs.hr.service.NoSuchCandidateException if a matching candidate could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Candidate findByPostingId_Last(
		long postingId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchCandidateException {
		return getPersistence()
				   .findByPostingId_Last(postingId, orderByComparator);
	}

	/**
	* Finds the candidates before and after the current candidate in the ordered set where postingId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param candidateId the primary key of the current candidate
	* @param postingId the posting ID to search with
	* @param orderByComparator the comparator to order the set by
	* @return the previous, current, and next candidate
	* @throws gov.ny.dfs.hr.service.NoSuchCandidateException if a candidate with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Candidate[] findByPostingId_PrevAndNext(
		long candidateId, long postingId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchCandidateException {
		return getPersistence()
				   .findByPostingId_PrevAndNext(candidateId, postingId,
			orderByComparator);
	}

	/**
	* Finds all the candidates where name = &#63;.
	*
	* @param name the name to search with
	* @return the matching candidates
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Candidate> findByName(
		java.lang.String name)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByName(name);
	}

	/**
	* Finds a range of all the candidates where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param name the name to search with
	* @param start the lower bound of the range of candidates to return
	* @param end the upper bound of the range of candidates to return (not inclusive)
	* @return the range of matching candidates
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Candidate> findByName(
		java.lang.String name, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByName(name, start, end);
	}

	/**
	* Finds an ordered range of all the candidates where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param name the name to search with
	* @param start the lower bound of the range of candidates to return
	* @param end the upper bound of the range of candidates to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching candidates
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Candidate> findByName(
		java.lang.String name, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByName(name, start, end, orderByComparator);
	}

	/**
	* Finds the first candidate in the ordered set where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param name the name to search with
	* @param orderByComparator the comparator to order the set by
	* @return the first matching candidate
	* @throws gov.ny.dfs.hr.service.NoSuchCandidateException if a matching candidate could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Candidate findByName_First(
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchCandidateException {
		return getPersistence().findByName_First(name, orderByComparator);
	}

	/**
	* Finds the last candidate in the ordered set where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param name the name to search with
	* @param orderByComparator the comparator to order the set by
	* @return the last matching candidate
	* @throws gov.ny.dfs.hr.service.NoSuchCandidateException if a matching candidate could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Candidate findByName_Last(
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchCandidateException {
		return getPersistence().findByName_Last(name, orderByComparator);
	}

	/**
	* Finds the candidates before and after the current candidate in the ordered set where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param candidateId the primary key of the current candidate
	* @param name the name to search with
	* @param orderByComparator the comparator to order the set by
	* @return the previous, current, and next candidate
	* @throws gov.ny.dfs.hr.service.NoSuchCandidateException if a candidate with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Candidate[] findByName_PrevAndNext(
		long candidateId, java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchCandidateException {
		return getPersistence()
				   .findByName_PrevAndNext(candidateId, name, orderByComparator);
	}

	/**
	* Finds all the candidates where email = &#63;.
	*
	* @param email the email to search with
	* @return the matching candidates
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Candidate> findByEmail(
		java.lang.String email)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByEmail(email);
	}

	/**
	* Finds a range of all the candidates where email = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param email the email to search with
	* @param start the lower bound of the range of candidates to return
	* @param end the upper bound of the range of candidates to return (not inclusive)
	* @return the range of matching candidates
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Candidate> findByEmail(
		java.lang.String email, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByEmail(email, start, end);
	}

	/**
	* Finds an ordered range of all the candidates where email = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param email the email to search with
	* @param start the lower bound of the range of candidates to return
	* @param end the upper bound of the range of candidates to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching candidates
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Candidate> findByEmail(
		java.lang.String email, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByEmail(email, start, end, orderByComparator);
	}

	/**
	* Finds the first candidate in the ordered set where email = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param email the email to search with
	* @param orderByComparator the comparator to order the set by
	* @return the first matching candidate
	* @throws gov.ny.dfs.hr.service.NoSuchCandidateException if a matching candidate could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Candidate findByEmail_First(
		java.lang.String email,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchCandidateException {
		return getPersistence().findByEmail_First(email, orderByComparator);
	}

	/**
	* Finds the last candidate in the ordered set where email = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param email the email to search with
	* @param orderByComparator the comparator to order the set by
	* @return the last matching candidate
	* @throws gov.ny.dfs.hr.service.NoSuchCandidateException if a matching candidate could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Candidate findByEmail_Last(
		java.lang.String email,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchCandidateException {
		return getPersistence().findByEmail_Last(email, orderByComparator);
	}

	/**
	* Finds the candidates before and after the current candidate in the ordered set where email = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param candidateId the primary key of the current candidate
	* @param email the email to search with
	* @param orderByComparator the comparator to order the set by
	* @return the previous, current, and next candidate
	* @throws gov.ny.dfs.hr.service.NoSuchCandidateException if a candidate with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Candidate[] findByEmail_PrevAndNext(
		long candidateId, java.lang.String email,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchCandidateException {
		return getPersistence()
				   .findByEmail_PrevAndNext(candidateId, email,
			orderByComparator);
	}

	/**
	* Finds all the candidates where phoneNum = &#63;.
	*
	* @param phoneNum the phone num to search with
	* @return the matching candidates
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Candidate> findByPhoneNum(
		java.lang.String phoneNum)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPhoneNum(phoneNum);
	}

	/**
	* Finds a range of all the candidates where phoneNum = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param phoneNum the phone num to search with
	* @param start the lower bound of the range of candidates to return
	* @param end the upper bound of the range of candidates to return (not inclusive)
	* @return the range of matching candidates
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Candidate> findByPhoneNum(
		java.lang.String phoneNum, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPhoneNum(phoneNum, start, end);
	}

	/**
	* Finds an ordered range of all the candidates where phoneNum = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param phoneNum the phone num to search with
	* @param start the lower bound of the range of candidates to return
	* @param end the upper bound of the range of candidates to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching candidates
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Candidate> findByPhoneNum(
		java.lang.String phoneNum, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByPhoneNum(phoneNum, start, end, orderByComparator);
	}

	/**
	* Finds the first candidate in the ordered set where phoneNum = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param phoneNum the phone num to search with
	* @param orderByComparator the comparator to order the set by
	* @return the first matching candidate
	* @throws gov.ny.dfs.hr.service.NoSuchCandidateException if a matching candidate could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Candidate findByPhoneNum_First(
		java.lang.String phoneNum,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchCandidateException {
		return getPersistence().findByPhoneNum_First(phoneNum, orderByComparator);
	}

	/**
	* Finds the last candidate in the ordered set where phoneNum = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param phoneNum the phone num to search with
	* @param orderByComparator the comparator to order the set by
	* @return the last matching candidate
	* @throws gov.ny.dfs.hr.service.NoSuchCandidateException if a matching candidate could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Candidate findByPhoneNum_Last(
		java.lang.String phoneNum,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchCandidateException {
		return getPersistence().findByPhoneNum_Last(phoneNum, orderByComparator);
	}

	/**
	* Finds the candidates before and after the current candidate in the ordered set where phoneNum = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param candidateId the primary key of the current candidate
	* @param phoneNum the phone num to search with
	* @param orderByComparator the comparator to order the set by
	* @return the previous, current, and next candidate
	* @throws gov.ny.dfs.hr.service.NoSuchCandidateException if a candidate with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Candidate[] findByPhoneNum_PrevAndNext(
		long candidateId, java.lang.String phoneNum,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchCandidateException {
		return getPersistence()
				   .findByPhoneNum_PrevAndNext(candidateId, phoneNum,
			orderByComparator);
	}

	/**
	* Finds all the candidates where postingId = &#63; and confirmationNum = &#63; and name = &#63; and email = &#63; and phoneNum = &#63;.
	*
	* @param postingId the posting ID to search with
	* @param confirmationNum the confirmation num to search with
	* @param name the name to search with
	* @param email the email to search with
	* @param phoneNum the phone num to search with
	* @return the matching candidates
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Candidate> findByP_C_N_E_Ph(
		long postingId, java.lang.String confirmationNum,
		java.lang.String name, java.lang.String email, java.lang.String phoneNum)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByP_C_N_E_Ph(postingId, confirmationNum, name, email,
			phoneNum);
	}

	/**
	* Finds a range of all the candidates where postingId = &#63; and confirmationNum = &#63; and name = &#63; and email = &#63; and phoneNum = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param postingId the posting ID to search with
	* @param confirmationNum the confirmation num to search with
	* @param name the name to search with
	* @param email the email to search with
	* @param phoneNum the phone num to search with
	* @param start the lower bound of the range of candidates to return
	* @param end the upper bound of the range of candidates to return (not inclusive)
	* @return the range of matching candidates
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Candidate> findByP_C_N_E_Ph(
		long postingId, java.lang.String confirmationNum,
		java.lang.String name, java.lang.String email,
		java.lang.String phoneNum, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByP_C_N_E_Ph(postingId, confirmationNum, name, email,
			phoneNum, start, end);
	}

	/**
	* Finds an ordered range of all the candidates where postingId = &#63; and confirmationNum = &#63; and name = &#63; and email = &#63; and phoneNum = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param postingId the posting ID to search with
	* @param confirmationNum the confirmation num to search with
	* @param name the name to search with
	* @param email the email to search with
	* @param phoneNum the phone num to search with
	* @param start the lower bound of the range of candidates to return
	* @param end the upper bound of the range of candidates to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching candidates
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Candidate> findByP_C_N_E_Ph(
		long postingId, java.lang.String confirmationNum,
		java.lang.String name, java.lang.String email,
		java.lang.String phoneNum, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByP_C_N_E_Ph(postingId, confirmationNum, name, email,
			phoneNum, start, end, orderByComparator);
	}

	/**
	* Finds the first candidate in the ordered set where postingId = &#63; and confirmationNum = &#63; and name = &#63; and email = &#63; and phoneNum = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param postingId the posting ID to search with
	* @param confirmationNum the confirmation num to search with
	* @param name the name to search with
	* @param email the email to search with
	* @param phoneNum the phone num to search with
	* @param orderByComparator the comparator to order the set by
	* @return the first matching candidate
	* @throws gov.ny.dfs.hr.service.NoSuchCandidateException if a matching candidate could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Candidate findByP_C_N_E_Ph_First(
		long postingId, java.lang.String confirmationNum,
		java.lang.String name, java.lang.String email,
		java.lang.String phoneNum,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchCandidateException {
		return getPersistence()
				   .findByP_C_N_E_Ph_First(postingId, confirmationNum, name,
			email, phoneNum, orderByComparator);
	}

	/**
	* Finds the last candidate in the ordered set where postingId = &#63; and confirmationNum = &#63; and name = &#63; and email = &#63; and phoneNum = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param postingId the posting ID to search with
	* @param confirmationNum the confirmation num to search with
	* @param name the name to search with
	* @param email the email to search with
	* @param phoneNum the phone num to search with
	* @param orderByComparator the comparator to order the set by
	* @return the last matching candidate
	* @throws gov.ny.dfs.hr.service.NoSuchCandidateException if a matching candidate could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Candidate findByP_C_N_E_Ph_Last(
		long postingId, java.lang.String confirmationNum,
		java.lang.String name, java.lang.String email,
		java.lang.String phoneNum,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchCandidateException {
		return getPersistence()
				   .findByP_C_N_E_Ph_Last(postingId, confirmationNum, name,
			email, phoneNum, orderByComparator);
	}

	/**
	* Finds the candidates before and after the current candidate in the ordered set where postingId = &#63; and confirmationNum = &#63; and name = &#63; and email = &#63; and phoneNum = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param candidateId the primary key of the current candidate
	* @param postingId the posting ID to search with
	* @param confirmationNum the confirmation num to search with
	* @param name the name to search with
	* @param email the email to search with
	* @param phoneNum the phone num to search with
	* @param orderByComparator the comparator to order the set by
	* @return the previous, current, and next candidate
	* @throws gov.ny.dfs.hr.service.NoSuchCandidateException if a candidate with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public gov.ny.dfs.hr.service.model.Candidate[] findByP_C_N_E_Ph_PrevAndNext(
		long candidateId, long postingId, java.lang.String confirmationNum,
		java.lang.String name, java.lang.String email,
		java.lang.String phoneNum,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			gov.ny.dfs.hr.service.NoSuchCandidateException {
		return getPersistence()
				   .findByP_C_N_E_Ph_PrevAndNext(candidateId, postingId,
			confirmationNum, name, email, phoneNum, orderByComparator);
	}

	/**
	* Finds all the candidates.
	*
	* @return the candidates
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Candidate> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Finds a range of all the candidates.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of candidates to return
	* @param end the upper bound of the range of candidates to return (not inclusive)
	* @return the range of candidates
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Candidate> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Finds an ordered range of all the candidates.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of candidates to return
	* @param end the upper bound of the range of candidates to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of candidates
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<gov.ny.dfs.hr.service.model.Candidate> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the candidates where postingId = &#63; from the database.
	*
	* @param postingId the posting ID to search with
	* @throws SystemException if a system exception occurred
	*/
	public void removeByPostingId(long postingId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByPostingId(postingId);
	}

	/**
	* Removes all the candidates where name = &#63; from the database.
	*
	* @param name the name to search with
	* @throws SystemException if a system exception occurred
	*/
	public void removeByName(java.lang.String name)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByName(name);
	}

	/**
	* Removes all the candidates where email = &#63; from the database.
	*
	* @param email the email to search with
	* @throws SystemException if a system exception occurred
	*/
	public void removeByEmail(java.lang.String email)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByEmail(email);
	}

	/**
	* Removes all the candidates where phoneNum = &#63; from the database.
	*
	* @param phoneNum the phone num to search with
	* @throws SystemException if a system exception occurred
	*/
	public void removeByPhoneNum(java.lang.String phoneNum)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByPhoneNum(phoneNum);
	}

	/**
	* Removes all the candidates where postingId = &#63; and confirmationNum = &#63; and name = &#63; and email = &#63; and phoneNum = &#63; from the database.
	*
	* @param postingId the posting ID to search with
	* @param confirmationNum the confirmation num to search with
	* @param name the name to search with
	* @param email the email to search with
	* @param phoneNum the phone num to search with
	* @throws SystemException if a system exception occurred
	*/
	public void removeByP_C_N_E_Ph(long postingId,
		java.lang.String confirmationNum, java.lang.String name,
		java.lang.String email, java.lang.String phoneNum)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence()
			.removeByP_C_N_E_Ph(postingId, confirmationNum, name, email,
			phoneNum);
	}

	/**
	* Removes all the candidates from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Counts all the candidates where postingId = &#63;.
	*
	* @param postingId the posting ID to search with
	* @return the number of matching candidates
	* @throws SystemException if a system exception occurred
	*/
	public int countByPostingId(long postingId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByPostingId(postingId);
	}

	/**
	* Counts all the candidates where name = &#63;.
	*
	* @param name the name to search with
	* @return the number of matching candidates
	* @throws SystemException if a system exception occurred
	*/
	public int countByName(java.lang.String name)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByName(name);
	}

	/**
	* Counts all the candidates where email = &#63;.
	*
	* @param email the email to search with
	* @return the number of matching candidates
	* @throws SystemException if a system exception occurred
	*/
	public int countByEmail(java.lang.String email)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByEmail(email);
	}

	/**
	* Counts all the candidates where phoneNum = &#63;.
	*
	* @param phoneNum the phone num to search with
	* @return the number of matching candidates
	* @throws SystemException if a system exception occurred
	*/
	public int countByPhoneNum(java.lang.String phoneNum)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByPhoneNum(phoneNum);
	}

	/**
	* Counts all the candidates where postingId = &#63; and confirmationNum = &#63; and name = &#63; and email = &#63; and phoneNum = &#63;.
	*
	* @param postingId the posting ID to search with
	* @param confirmationNum the confirmation num to search with
	* @param name the name to search with
	* @param email the email to search with
	* @param phoneNum the phone num to search with
	* @return the number of matching candidates
	* @throws SystemException if a system exception occurred
	*/
	public int countByP_C_N_E_Ph(long postingId,
		java.lang.String confirmationNum, java.lang.String name,
		java.lang.String email, java.lang.String phoneNum)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByP_C_N_E_Ph(postingId, confirmationNum, name, email,
			phoneNum);
	}

	/**
	* Counts all the candidates.
	*
	* @return the number of candidates
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	private CandidatePersistence getPersistence() {
		return getCandidatePersistence();
	}

}