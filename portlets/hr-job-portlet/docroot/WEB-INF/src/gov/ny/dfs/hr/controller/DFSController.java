package gov.ny.dfs.hr.controller;

import javax.portlet.RenderRequest;

import org.springframework.web.bind.annotation.ExceptionHandler;

public abstract class DFSController {
	
	@ExceptionHandler(Exception.class)
	public String handleException(Exception ex, RenderRequest request) {
		request.setAttribute("exception", ex);
		return "error";
	}

}
