package gov.ny.dfs.hr.model.application;

import net.sf.oval.constraint.*;

/**
 * @author Abey
 * 
 */


public class CandidateForm {

	@NotEmpty(message = "{err.candidate.name.notEmpty}")
	@NotNull(message = "{err.candidate.name.notNull}")
	@Max(value = 50, message = "{err.candidate.name.max}")
	@Min(value = 1, message = "{err.candidate.name.min}")
	private String name;
	
	@Email(message="{err.candidate.email.notValid}")
	@NotEmpty(message = "{err.candidate.email.notEmpty}")
	private String email;

	@Email(message="{err.candidate.email.notValid}")
	@NotEmpty(message = "{err.candidate.email.notEmpty}")
	@EqualToField(value="email", message="{err.candidate.confirmEmail.matches}")
	private String confirmEmail;
	
	@MatchPattern(pattern="\\d{3}-\\d{3}-\\d{4}", message="{err.candidate.phoneNum.notValid}")
	@NotEmpty(message = "{err.candidate.phoneNum.notEmpty}")
	private String phoneNum;

	private String resume;
	
	private String coverLetter;

}
