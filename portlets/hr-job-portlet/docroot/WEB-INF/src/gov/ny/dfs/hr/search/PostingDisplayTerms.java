/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package gov.ny.dfs.hr.search;

import com.liferay.portal.kernel.dao.search.DisplayTerms;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.PortletRequest;

/**
 * @author Brian Wing Shun Chan
 */
public class PostingDisplayTerms extends DisplayTerms {

	public static final String DESCRIPTION = "description";

	public static final String TITLE = "title";
	public static final String JOB_ID = "jobId";

	public PostingDisplayTerms(PortletRequest portletRequest) {
		super(portletRequest);

		description = ParamUtil.getString(portletRequest, DESCRIPTION);
		title = ParamUtil.getString(portletRequest, TITLE);
		jobId = ParamUtil.getString(portletRequest, JOB_ID);
	}

	public String getDescription() {
		return description;
	}

	public String getTitle() {
		return title;
	}

	public String getJobId() {
		return jobId;
	}

	protected String description;
	protected String title;
	protected String jobId;

}