
/*$(document).ready(function() {
	$("#jexpand tr:odd").addClass("odd");
	$("#jexpand tr:not(.odd)").hide();
	$("#jexpand tr:first-child").show();

	$("#jexpand tr.odd").click(function() {
		$(this).next("tr").toggle();
		$(this).find(".arrow").toggleClass("up");
	});
	// $("#jexpand").jExpand();
});
*/

/*
 * display all the fields of an obj.
 * 
 * http://jsfiddle.net/abeym/j9PW2/43/
 * 
 *    tip: jsfiddle uses frames. below lines are requiredi n html frame  to call a function from different frames.
 *     var dispAll;
 *     var du;
*/

dispAll=function dispAll(obj)
{
    var debug=true, output='console';
    du(obj, output, debug);
    return false;
};

//debug utils
du=function(obj, output, debug){
    /*
     * output = html, console, pop
     * debug =true,false
    */
    this.output = (typeof output == 'undefined')?'pop':output;
    this.debug = (typeof debug == 'undefined')?true:debug;
    str = function(obj, nl)
    {
        var str='', i=0, ierr=0;
        for(var x in obj)
        {
            i++;
            str += nl+i+'::============='+nl+x+nl;
            try{
                str+=nl+'obj.x-------------'+nl+obj.x+nl;
            }
            catch(err)
            {
                ierr++;
                str+=nl+'err::^^^^^^^^^^^^^'+nl+err+nl;
            }
            try{
                str+=nl+'obj[x]::-------------'+nl+obj[x]+nl;
            }
            catch(err)
            {
                ierr++;
                str+=nl+'err::^^^^^^^^^^^^^'+nl+err+nl;
            }
        }
        str ='Total:'+i+'. Errors:'+ierr+nl+str;
        return str;
    };
    this.console = function(obj){
        if (window.console && window.console.firebug) {
            window.console.log(str(obj,'\n'));
        }
        else if (window.parent.console &&
                 window.parent.console.firebug) {
            window.parent.console.log(str(obj,'\n'));
        }
    };
    this.pop = function(obj){
        alert(str(obj,'\n'));  
    };
    this.html = function(obj){
        $('body').append(str(obj,'<br/>'));
    };
    if(this.debug)
    {
       this[this.output](obj);
    }
};


