<%@page import="gov.ny.dfs.liferay.idw.service.service.InstitutionLocalServiceUtil"%>
<%@page import="gov.ny.dfs.liferay.idw.service.model.Institution"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>


<liferay-theme:defineObjects />

<portlet:defineObjects />

The List of <b>Insitutions</b>.

<liferay-ui:search-container emptyResultsMessage="There are no Insitutions to display" >
    <liferay-ui:search-container-results
        results="<%= InstitutionLocalServiceUtil.findAll() %>"
        total="<%= InstitutionLocalServiceUtil.countAll() %>"
        
    />

    <liferay-ui:search-container-row
        className="gov.ny.dfs.liferay.idw.service.model.Institution"
        keyProperty="institutionId"
        modelVar="institution" escapedModel="<%= true %>"
    >
        <liferay-ui:search-container-column-text
            name="name"
            value="<%= institution.getName() %>"
        />

        <liferay-ui:search-container-column-text
            name="Incorporated"
            value="<%= String.valueOf(institution.getIncorporated()) %>"
        />

        <liferay-ui:search-container-column-text
            name="status"
            value="<%= institution.getStatus() %>"
        />

        <liferay-ui:search-container-column-text
            name="type"
            value="<%= institution.getType() %>"
        />

    </liferay-ui:search-container-row>

    <liferay-ui:search-iterator  paginate="true" />
</liferay-ui:search-container>
