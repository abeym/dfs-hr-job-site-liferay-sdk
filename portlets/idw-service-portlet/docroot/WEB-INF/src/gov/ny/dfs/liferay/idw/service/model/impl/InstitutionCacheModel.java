/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package gov.ny.dfs.liferay.idw.service.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import gov.ny.dfs.liferay.idw.service.model.Institution;

import java.util.Date;

/**
 * The cache model class for representing Institution in entity cache.
 *
 * @author Abey
 * @see Institution
 * @generated
 */
public class InstitutionCacheModel implements CacheModel<Institution> {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{institutionId=");
		sb.append(institutionId);
		sb.append(", name=");
		sb.append(name);
		sb.append(", incorporated=");
		sb.append(incorporated);
		sb.append(", type=");
		sb.append(type);
		sb.append(", subType=");
		sb.append(subType);
		sb.append(", status=");
		sb.append(status);
		sb.append(", subStatus=");
		sb.append(subStatus);
		sb.append("}");

		return sb.toString();
	}

	public Institution toEntityModel() {
		InstitutionImpl institutionImpl = new InstitutionImpl();

		if (institutionId == null) {
			institutionImpl.setInstitutionId(StringPool.BLANK);
		}
		else {
			institutionImpl.setInstitutionId(institutionId);
		}

		if (name == null) {
			institutionImpl.setName(StringPool.BLANK);
		}
		else {
			institutionImpl.setName(name);
		}

		if (incorporated == Long.MIN_VALUE) {
			institutionImpl.setIncorporated(null);
		}
		else {
			institutionImpl.setIncorporated(new Date(incorporated));
		}

		if (type == null) {
			institutionImpl.setType(StringPool.BLANK);
		}
		else {
			institutionImpl.setType(type);
		}

		if (subType == null) {
			institutionImpl.setSubType(StringPool.BLANK);
		}
		else {
			institutionImpl.setSubType(subType);
		}

		if (status == null) {
			institutionImpl.setStatus(StringPool.BLANK);
		}
		else {
			institutionImpl.setStatus(status);
		}

		if (subStatus == null) {
			institutionImpl.setSubStatus(StringPool.BLANK);
		}
		else {
			institutionImpl.setSubStatus(subStatus);
		}

		institutionImpl.resetOriginalValues();

		return institutionImpl;
	}

	public String institutionId;
	public String name;
	public long incorporated;
	public String type;
	public String subType;
	public String status;
	public String subStatus;
}