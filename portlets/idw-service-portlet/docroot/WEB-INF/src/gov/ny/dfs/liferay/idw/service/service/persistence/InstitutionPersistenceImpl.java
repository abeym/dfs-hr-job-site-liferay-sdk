/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package gov.ny.dfs.liferay.idw.service.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import gov.ny.dfs.liferay.idw.service.NoSuchInstitutionException;
import gov.ny.dfs.liferay.idw.service.model.Institution;
import gov.ny.dfs.liferay.idw.service.model.impl.InstitutionImpl;
import gov.ny.dfs.liferay.idw.service.model.impl.InstitutionModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the institution service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Abey
 * @see InstitutionPersistence
 * @see InstitutionUtil
 * @generated
 */
public class InstitutionPersistenceImpl extends BasePersistenceImpl<Institution>
	implements InstitutionPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link InstitutionUtil} to access the institution persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = InstitutionImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST = FINDER_CLASS_NAME_ENTITY +
		".List";
	public static final FinderPath FINDER_PATH_FIND_BY_NAME = new FinderPath(InstitutionModelImpl.ENTITY_CACHE_ENABLED,
			InstitutionModelImpl.FINDER_CACHE_ENABLED, InstitutionImpl.class,
			FINDER_CLASS_NAME_LIST, "findByname",
			new String[] {
				String.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_COUNT_BY_NAME = new FinderPath(InstitutionModelImpl.ENTITY_CACHE_ENABLED,
			InstitutionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST, "countByname",
			new String[] { String.class.getName() });
	public static final FinderPath FINDER_PATH_FIND_ALL = new FinderPath(InstitutionModelImpl.ENTITY_CACHE_ENABLED,
			InstitutionModelImpl.FINDER_CACHE_ENABLED, InstitutionImpl.class,
			FINDER_CLASS_NAME_LIST, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(InstitutionModelImpl.ENTITY_CACHE_ENABLED,
			InstitutionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST, "countAll", new String[0]);

	/**
	 * Caches the institution in the entity cache if it is enabled.
	 *
	 * @param institution the institution to cache
	 */
	public void cacheResult(Institution institution) {
		EntityCacheUtil.putResult(InstitutionModelImpl.ENTITY_CACHE_ENABLED,
			InstitutionImpl.class, institution.getPrimaryKey(), institution);

		institution.resetOriginalValues();
	}

	/**
	 * Caches the institutions in the entity cache if it is enabled.
	 *
	 * @param institutions the institutions to cache
	 */
	public void cacheResult(List<Institution> institutions) {
		for (Institution institution : institutions) {
			if (EntityCacheUtil.getResult(
						InstitutionModelImpl.ENTITY_CACHE_ENABLED,
						InstitutionImpl.class, institution.getPrimaryKey(), this) == null) {
				cacheResult(institution);
			}
		}
	}

	/**
	 * Clears the cache for all institutions.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(InstitutionImpl.class.getName());
		}

		EntityCacheUtil.clearCache(InstitutionImpl.class.getName());
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);
	}

	/**
	 * Clears the cache for the institution.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Institution institution) {
		EntityCacheUtil.removeResult(InstitutionModelImpl.ENTITY_CACHE_ENABLED,
			InstitutionImpl.class, institution.getPrimaryKey());
	}

	/**
	 * Creates a new institution with the primary key. Does not add the institution to the database.
	 *
	 * @param institutionId the primary key for the new institution
	 * @return the new institution
	 */
	public Institution create(String institutionId) {
		Institution institution = new InstitutionImpl();

		institution.setNew(true);
		institution.setPrimaryKey(institutionId);

		return institution;
	}

	/**
	 * Removes the institution with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the institution to remove
	 * @return the institution that was removed
	 * @throws com.liferay.portal.NoSuchModelException if a institution with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Institution remove(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return remove((String)primaryKey);
	}

	/**
	 * Removes the institution with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param institutionId the primary key of the institution to remove
	 * @return the institution that was removed
	 * @throws gov.ny.dfs.liferay.idw.service.NoSuchInstitutionException if a institution with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Institution remove(String institutionId)
		throws NoSuchInstitutionException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Institution institution = (Institution)session.get(InstitutionImpl.class,
					institutionId);

			if (institution == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + institutionId);
				}

				throw new NoSuchInstitutionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					institutionId);
			}

			return institutionPersistence.remove(institution);
		}
		catch (NoSuchInstitutionException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Removes the institution from the database. Also notifies the appropriate model listeners.
	 *
	 * @param institution the institution to remove
	 * @return the institution that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Institution remove(Institution institution)
		throws SystemException {
		return super.remove(institution);
	}

	@Override
	protected Institution removeImpl(Institution institution)
		throws SystemException {
		institution = toUnwrappedModel(institution);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, institution);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

		EntityCacheUtil.removeResult(InstitutionModelImpl.ENTITY_CACHE_ENABLED,
			InstitutionImpl.class, institution.getPrimaryKey());

		return institution;
	}

	@Override
	public Institution updateImpl(
		gov.ny.dfs.liferay.idw.service.model.Institution institution,
		boolean merge) throws SystemException {
		institution = toUnwrappedModel(institution);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, institution, merge);

			institution.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

		EntityCacheUtil.putResult(InstitutionModelImpl.ENTITY_CACHE_ENABLED,
			InstitutionImpl.class, institution.getPrimaryKey(), institution);

		return institution;
	}

	protected Institution toUnwrappedModel(Institution institution) {
		if (institution instanceof InstitutionImpl) {
			return institution;
		}

		InstitutionImpl institutionImpl = new InstitutionImpl();

		institutionImpl.setNew(institution.isNew());
		institutionImpl.setPrimaryKey(institution.getPrimaryKey());

		institutionImpl.setInstitutionId(institution.getInstitutionId());
		institutionImpl.setName(institution.getName());
		institutionImpl.setIncorporated(institution.getIncorporated());
		institutionImpl.setType(institution.getType());
		institutionImpl.setSubType(institution.getSubType());
		institutionImpl.setStatus(institution.getStatus());
		institutionImpl.setSubStatus(institution.getSubStatus());

		return institutionImpl;
	}

	/**
	 * Finds the institution with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the institution to find
	 * @return the institution
	 * @throws com.liferay.portal.NoSuchModelException if a institution with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Institution findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey((String)primaryKey);
	}

	/**
	 * Finds the institution with the primary key or throws a {@link gov.ny.dfs.liferay.idw.service.NoSuchInstitutionException} if it could not be found.
	 *
	 * @param institutionId the primary key of the institution to find
	 * @return the institution
	 * @throws gov.ny.dfs.liferay.idw.service.NoSuchInstitutionException if a institution with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Institution findByPrimaryKey(String institutionId)
		throws NoSuchInstitutionException, SystemException {
		Institution institution = fetchByPrimaryKey(institutionId);

		if (institution == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + institutionId);
			}

			throw new NoSuchInstitutionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				institutionId);
		}

		return institution;
	}

	/**
	 * Finds the institution with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the institution to find
	 * @return the institution, or <code>null</code> if a institution with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Institution fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey((String)primaryKey);
	}

	/**
	 * Finds the institution with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param institutionId the primary key of the institution to find
	 * @return the institution, or <code>null</code> if a institution with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Institution fetchByPrimaryKey(String institutionId)
		throws SystemException {
		Institution institution = (Institution)EntityCacheUtil.getResult(InstitutionModelImpl.ENTITY_CACHE_ENABLED,
				InstitutionImpl.class, institutionId, this);

		if (institution == _nullInstitution) {
			return null;
		}

		if (institution == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				institution = (Institution)session.get(InstitutionImpl.class,
						institutionId);
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (institution != null) {
					cacheResult(institution);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(InstitutionModelImpl.ENTITY_CACHE_ENABLED,
						InstitutionImpl.class, institutionId, _nullInstitution);
				}

				closeSession(session);
			}
		}

		return institution;
	}

	/**
	 * Finds all the institutions where name = &#63;.
	 *
	 * @param name the name to search with
	 * @return the matching institutions
	 * @throws SystemException if a system exception occurred
	 */
	public List<Institution> findByname(String name) throws SystemException {
		return findByname(name, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Finds a range of all the institutions where name = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param name the name to search with
	 * @param start the lower bound of the range of institutions to return
	 * @param end the upper bound of the range of institutions to return (not inclusive)
	 * @return the range of matching institutions
	 * @throws SystemException if a system exception occurred
	 */
	public List<Institution> findByname(String name, int start, int end)
		throws SystemException {
		return findByname(name, start, end, null);
	}

	/**
	 * Finds an ordered range of all the institutions where name = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param name the name to search with
	 * @param start the lower bound of the range of institutions to return
	 * @param end the upper bound of the range of institutions to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching institutions
	 * @throws SystemException if a system exception occurred
	 */
	public List<Institution> findByname(String name, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] {
				name,
				
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<Institution> list = (List<Institution>)FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_NAME,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_INSTITUTION_WHERE);

			if (name == null) {
				query.append(_FINDER_COLUMN_NAME_NAME_1);
			}
			else {
				if (name.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_NAME_NAME_3);
				}
				else {
					query.append(_FINDER_COLUMN_NAME_NAME_2);
				}
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(InstitutionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (name != null) {
					qPos.add(name);
				}

				list = (List<Institution>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_BY_NAME,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_NAME,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Finds the first institution in the ordered set where name = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param name the name to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the first matching institution
	 * @throws gov.ny.dfs.liferay.idw.service.NoSuchInstitutionException if a matching institution could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Institution findByname_First(String name,
		OrderByComparator orderByComparator)
		throws NoSuchInstitutionException, SystemException {
		List<Institution> list = findByname(name, 0, 1, orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("name=");
			msg.append(name);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchInstitutionException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the last institution in the ordered set where name = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param name the name to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the last matching institution
	 * @throws gov.ny.dfs.liferay.idw.service.NoSuchInstitutionException if a matching institution could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Institution findByname_Last(String name,
		OrderByComparator orderByComparator)
		throws NoSuchInstitutionException, SystemException {
		int count = countByname(name);

		List<Institution> list = findByname(name, count - 1, count,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("name=");
			msg.append(name);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchInstitutionException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the institutions before and after the current institution in the ordered set where name = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param institutionId the primary key of the current institution
	 * @param name the name to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the previous, current, and next institution
	 * @throws gov.ny.dfs.liferay.idw.service.NoSuchInstitutionException if a institution with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Institution[] findByname_PrevAndNext(String institutionId,
		String name, OrderByComparator orderByComparator)
		throws NoSuchInstitutionException, SystemException {
		Institution institution = findByPrimaryKey(institutionId);

		Session session = null;

		try {
			session = openSession();

			Institution[] array = new InstitutionImpl[3];

			array[0] = getByname_PrevAndNext(session, institution, name,
					orderByComparator, true);

			array[1] = institution;

			array[2] = getByname_PrevAndNext(session, institution, name,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Institution getByname_PrevAndNext(Session session,
		Institution institution, String name,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_INSTITUTION_WHERE);

		if (name == null) {
			query.append(_FINDER_COLUMN_NAME_NAME_1);
		}
		else {
			if (name.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NAME_NAME_3);
			}
			else {
				query.append(_FINDER_COLUMN_NAME_NAME_2);
			}
		}

		if (orderByComparator != null) {
			String[] orderByFields = orderByComparator.getOrderByFields();

			if (orderByFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(InstitutionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (name != null) {
			qPos.add(name);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByValues(institution);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Institution> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Finds all the institutions.
	 *
	 * @return the institutions
	 * @throws SystemException if a system exception occurred
	 */
	public List<Institution> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Finds a range of all the institutions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of institutions to return
	 * @param end the upper bound of the range of institutions to return (not inclusive)
	 * @return the range of institutions
	 * @throws SystemException if a system exception occurred
	 */
	public List<Institution> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Finds an ordered range of all the institutions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of institutions to return
	 * @param end the upper bound of the range of institutions to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of institutions
	 * @throws SystemException if a system exception occurred
	 */
	public List<Institution> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] {
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<Institution> list = (List<Institution>)FinderCacheUtil.getResult(FINDER_PATH_FIND_ALL,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_INSTITUTION);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_INSTITUTION.concat(InstitutionModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Institution>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Institution>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_ALL,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_ALL, finderArgs,
						list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the institutions where name = &#63; from the database.
	 *
	 * @param name the name to search with
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByname(String name) throws SystemException {
		for (Institution institution : findByname(name)) {
			institutionPersistence.remove(institution);
		}
	}

	/**
	 * Removes all the institutions from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Institution institution : findAll()) {
			institutionPersistence.remove(institution);
		}
	}

	/**
	 * Counts all the institutions where name = &#63;.
	 *
	 * @param name the name to search with
	 * @return the number of matching institutions
	 * @throws SystemException if a system exception occurred
	 */
	public int countByname(String name) throws SystemException {
		Object[] finderArgs = new Object[] { name };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_NAME,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_INSTITUTION_WHERE);

			if (name == null) {
				query.append(_FINDER_COLUMN_NAME_NAME_1);
			}
			else {
				if (name.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_NAME_NAME_3);
				}
				else {
					query.append(_FINDER_COLUMN_NAME_NAME_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (name != null) {
					qPos.add(name);
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_NAME,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Counts all the institutions.
	 *
	 * @return the number of institutions
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Object[] finderArgs = new Object[0];

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				finderArgs, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_INSTITUTION);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL, finderArgs,
					count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the institution persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.gov.ny.dfs.liferay.idw.service.model.Institution")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Institution>> listenersList = new ArrayList<ModelListener<Institution>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Institution>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(InstitutionImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST);
	}

	@BeanReference(type = InstitutionPersistence.class)
	protected InstitutionPersistence institutionPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_INSTITUTION = "SELECT institution FROM Institution institution";
	private static final String _SQL_SELECT_INSTITUTION_WHERE = "SELECT institution FROM Institution institution WHERE ";
	private static final String _SQL_COUNT_INSTITUTION = "SELECT COUNT(institution) FROM Institution institution";
	private static final String _SQL_COUNT_INSTITUTION_WHERE = "SELECT COUNT(institution) FROM Institution institution WHERE ";
	private static final String _FINDER_COLUMN_NAME_NAME_1 = "institution.name IS NULL";
	private static final String _FINDER_COLUMN_NAME_NAME_2 = "institution.name = ?";
	private static final String _FINDER_COLUMN_NAME_NAME_3 = "(institution.name IS NULL OR institution.name = ?)";
	private static final String _ORDER_BY_ENTITY_ALIAS = "institution.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Institution exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Institution exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(InstitutionPersistenceImpl.class);
	private static Institution _nullInstitution = new InstitutionImpl() {
			public Object clone() {
				return this;
			}

			public CacheModel<Institution> toCacheModel() {
				return _nullInstitutionCacheModel;
			}
		};

	private static CacheModel<Institution> _nullInstitutionCacheModel = new CacheModel<Institution>() {
			public Institution toEntityModel() {
				return _nullInstitution;
			}
		};
}