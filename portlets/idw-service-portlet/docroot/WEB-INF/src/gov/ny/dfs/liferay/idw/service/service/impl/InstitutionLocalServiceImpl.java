/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package gov.ny.dfs.liferay.idw.service.service.impl;

import com.liferay.portal.kernel.exception.SystemException;

import gov.ny.dfs.liferay.idw.service.model.Institution;
import gov.ny.dfs.liferay.idw.service.service.base.InstitutionLocalServiceBaseImpl;

import java.util.List;

/**
 * The implementation of the institution local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link gov.ny.dfs.liferay.idw.service.service.InstitutionLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Abey
 * @see gov.ny.dfs.liferay.idw.service.service.base.InstitutionLocalServiceBaseImpl
 * @see gov.ny.dfs.liferay.idw.service.service.InstitutionLocalServiceUtil
 */
public class InstitutionLocalServiceImpl extends InstitutionLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link gov.ny.dfs.liferay.idw.service.service.InstitutionLocalServiceUtil} to access the institution local service.
	 */
	
	public List<Institution> findAll() throws SystemException
	{
		return institutionPersistence.findAll();
	}
	
	public int countAll() throws SystemException
	{
		return institutionPersistence.countAll();
	}
}