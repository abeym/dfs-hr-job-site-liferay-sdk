package com.sympo.antivirus;

import com.liferay.portal.kernel.exception.PortalException;

/**
 * @author Michael C. Han
 */
public class AntivirusScannerException extends PortalException {

	public AntivirusScannerException() {
		super();
	}

	public AntivirusScannerException(String msg) {
		super(msg);
	}

	public AntivirusScannerException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public AntivirusScannerException(Throwable cause) {
		super(cause);
	}

}