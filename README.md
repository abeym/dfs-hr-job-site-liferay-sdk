# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Recruitment portlet
* 1.1

### How do I get set up? ###

* [Summary of set up](https://bitbucket.org/abeym/liferay-sdk-dfs/issue/57/hr-app-portlet-setup)

* Configuration

portal-ext.properties
javax.persistence.validation.mode=NONE


* Deployment instructions
build the war and deploy on a liferay 6.0 sp2 bundle

### Contribution guidelines ###

* Code review [Issues](https://bitbucket.org/abeym/liferay-sdk-dfs/issues)

### Who do I talk to? ###

* Abey M @[bitbucket](https://bitbucket.org/abeym)
* Abey M @[github](https://github.com/abeym)